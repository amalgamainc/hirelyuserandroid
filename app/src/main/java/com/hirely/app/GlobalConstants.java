package com.hirely.app;

public class GlobalConstants {

    //Feedback
    public static final String SERVICE_ID = "service_id";
    public static final String COMMENT = "comments";
    public static final String RATING = "rating";
    public static final String IS_PROVIDER = "is_provider";
    public static String url = "http://1.yoo.lviv.ua/index.php/Webservices";
    public static String PREF_NAME = "com.hirely.app";
    public static String APP_WORKING_IN_BACKGROUND = "app_background";
    //-----------------REGISTETR API KEYS------------------
    public static String USERNAME = "username";
    public static String EMAIL = "email";
    public static String PASSWORD = "password";
    public static String VERIFYMOB = "mobileno";
    public static String DEVICEID = "device_id";
    public static String DEVICETYPE = "device_type";
    public static String LATITUDE = "lat";
    public static String LONGITUDE = "lon";
    public static String STATUS = "status";
    public static String CODE = "code";
    //-----------------Login API KEYS------------------
    public static String loginuser = "username";
    public static String loginpassword = "password";
    public static String USERTYPE = "type";
    public static String PROVIDERID = "id";
    //-----------------Cancel request API KEYS------------------
    public static String USERSTATUS = "status";
    //-----------------Services API KEYS------------------
    public static String SERVICETYPE = "service_type";
    public static String FLR = "Flr";
    public static String BUILDING = "building";
    public static String STREET = "street";
    public static String CITY = "city";
    public static String LANDMARK = "landmark";
    public static String EXTRA = "extra[]";
    public static String COST = "cost";
    //-----------------User ProfileFragment API KEYS------------------
    public static String USERID = "id";
    //-----------------Feedback API KEYS------------------
    public static String FeedUSERID = "user_id";
    public static String FeedJOBID = "job_id";
    public static String FeedPROVIDERID = "provider_id";
    public static String FeedSERVICETYPE = "service_type";
    public static String FeedCOMMENTS = "comments";
    public static String FeedRATING = "rating";
    //-----------------Social Login API KEYS------------------
    public static String SOCIALUSERNAME = "username";
    public static String SOCIALEMAIL = "email";
    public static String SOCIALUNIQUEID = "unique_id";
    //-----------------User Notification API KEYS------------------
    public static String uNotificationId = "id";
    public static String uNotificationLat = "lat";
    public static String uNotificationLon = "lon";
    public static String uNotificationAddress = "address";
    public static String uNotificationServiceType = "service_type";
    public static String uNotificationExtra = "extra[]";
    //-----------------Provider Notification API KEYS------------------
    public static String pNotificationId = "id";
    public static String pNotificationLat = "lat";
    public static String pNotificationLon = "lon";
    public static String pNotificationServiceType = "service_type";

    //-----------------Forget Password API KEYS------------------
    public static String FORGET_EMAIL = "email";
    public static String FORGET_PASS = "password";

    //-----------------CATEGORY INFO API KEYS------------------
    public static String CATEGORY_NAME = "cat_name";
}