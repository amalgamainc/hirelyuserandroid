package com.hirely.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.hirely.app.activity.LogInActivity;

import java.util.HashMap;

public class SessionManager {

    private SessionManager() {
    }

    private static SharedPreferences getPrefs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void createLoginSession(Context c, String name, String pass) {
        getPrefs(c).edit().putString("rememberEmail", name).commit();
        getPrefs(c).edit().putString("rememberPassword", pass).commit();
        getPrefs(c).edit().putBoolean("isLoggedIn", true).commit();
    }

    public static void logoutUser(Context c) {
        getPrefs(c).edit().clear().commit();
        Intent i = new Intent(c, LogInActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        c.startActivity(i);
    }

    public static boolean isLoggedIn(Context c) {
        return getPrefs(c).getBoolean("isLoggedIn", false);
    }
}