/*
package com.hirely.app.fragment.archive;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.app.Global;
import com.hirely.app.R;
import com.hirely.app.fragment.MassageServiceFragment;
import com.hirely.app.util.WebServiceHandler;

import java.util.ArrayList;
import java.util.HashMap;

*/
/*import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;*//*


@SuppressLint({"HandlerLeak", "NewApi"})
public class HirelyServicesFragment extends Fragment {
    View rootView;
    Global glob;
    ListView servicelist_listview;
    ProgressDialog pd;
    */
/*
     * private static final String TAG = "PAYPAL PAYMENT"; private static final
     * String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
     * private static final String CONFIG_CLIENT_ID =
     * "ATNmAd3PrhY5ySoza9ujVTVMkcqQMn_6KLRt2vaUEMjHzQTQ898grzHdHlBygv6T4lGAc70gwkFc28H_";
     * private static final int REQUEST_CODE_PAYMENT = 1; private static
     * PayPalConfiguration config = new PayPalConfiguration()
     * .environment(CONFIG_ENVIRONMENT) .clientId(CONFIG_CLIENT_ID)
     * .merchantName("Priyanka Sharma")
     * .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
     * .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
     * PayPalPayment thingToBuy;
     *//*

    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            pd.dismiss();
            if (res.equalsIgnoreCase("1")) {
                servicelist_listview.setAdapter(new ListAdap(getActivity(), glob.getServiceList()));
            } else {
                Toast.makeText(getActivity(), "No Service Available", Toast.LENGTH_LONG).show();
            }
        }
    };
    private Runnable servicelistThread = new Runnable() {
        @Override
        public void run() {
            String res = "";
            try {
                res = WebServiceHandler.getCost(getActivity());
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            handler.sendMessage(msg);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_hirely__services, container, false);
        glob = (Global) getActivity().getApplicationContext();

        servicelist_listview = (ListView) rootView.findViewById(R.id.list);

        pd = ProgressDialog.show(getActivity(), "", "Loading Services...");
        new Thread(null, servicelistThread, " ").start();

		*/
/*
         * Intent intent = new Intent(getActivity(), PayPalService.class);
		 * intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
		 * getActivity().startService(intent);
		 *//*


        servicelist_listview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String serviceType = glob.getServiceList().get(position).get("cat_name");
                glob.setServicetype(serviceType);
                String serviceCost = glob.getServiceList().get(position).get("cost");
                glob.setServicecost(serviceCost);
                String estmatedCost = glob.getServiceList().get(position).get("Estimated_cost");
                glob.setEstimatedcost(estmatedCost);
                Log.e("global",
                        "service Type" + glob.getServicetype() + glob.getEstimatedcost() + " " + glob.getServicecost());
                startSelectedService(glob.getServicetype());

				*/
/*
                 * thingToBuy = new PayPalPayment(new
				 * BigDecimal(""+glob.getServicecost()), "USD",
				 * glob.getUsername(), PayPalPayment.PAYMENT_INTENT_SALE);
				 * Intent intent = new Intent(getActivity(),
				 * PaymentActivity.class);
				 * intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
				 * getActivity().startActivityForResult(intent,
				 * REQUEST_CODE_PAYMENT);
				 *//*

            }
        });

        return rootView;
    }

    */
/*
     * /*/
/*********************
 * PAYPAL PAYMENT **************************
 *
 * @Override public void onActivityResult(int requestCode, int resultCode,
 * Intent data) { if (requestCode == REQUEST_CODE_PAYMENT) { if (resultCode
 * == Activity.RESULT_OK) { PaymentConfirmation confirm =
 * data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION); if
 * (confirm != null) { try { Log.i(TAG, confirm.toJSONObject().toString(4));
 * Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
 * Toast.makeText(getActivity(),
 * "Payment Confirmation info received from PayPal",
 * Toast.LENGTH_LONG).show();
 * <p/>
 * <p/>
 * } catch (JSONException e) { Log.e(TAG,
 * "an extremely unlikely failure occurred: ", e); } } } else if (resultCode
 * == Activity.RESULT_CANCELED) { Log.i(TAG, "The user canceled."); } else
 * if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) { Log.i(TAG,
 * "An invalid Payment or PayPalConfiguration was submitted. Please see the docs."
 * ); } } }
 *//*

    private void startSelectedService(String service_selected) {

        if (service_selected.equalsIgnoreCase("CONDO CLEANING")) {
            Fragment condo = new CandoCleaningFragment();
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, condo).addToBackStack(null).commit();
        } else if (service_selected.equalsIgnoreCase("LAUNDRY")) {
            Fragment laundry = new LaundryFragment();
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, laundry).addToBackStack(null).commit();
        } else if (service_selected.equalsIgnoreCase("WATER DELIVERY")) {
            Fragment water = new WaterDeliveryFragment();
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, water).addToBackStack(null).commit();
        } else {
            Fragment massage = new MassageServiceFragment();
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, massage).addToBackStack(null).commit();
        }

    }

    public void onPause() {
        super.onPause();
    }

    // ************************ LIST ADAPTER CLASS ***********************
    @SuppressLint({"ViewHolder", "InflateParams"})
    class ListAdap extends BaseAdapter {
        public ArrayList<HashMap<String, String>> ServiceList;
        LayoutInflater inflater;
        TextView service_txt;
        private Activity context;

        public ListAdap(Activity context, ArrayList<HashMap<String, String>> jobList) {
            super();
            this.context = context;
            this.ServiceList = jobList;
        }

        @Override
        public int getCount() {
            return ServiceList.size();
        }

        @Override
        public Object getItem(int position) {
            return ServiceList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View rootview = inflater.inflate(R.layout.customlistservicees, null, true);

            service_txt = (TextView) rootview.findViewById(R.id.service_txt);

            String catname = ServiceList.get(position).get("cat_name");
            String cost = ServiceList.get(position).get("cost");
            String estimation = ServiceList.get(position).get("Estimated_cost");

            String newlist = catname;
            // String newlist = catname +" "+ "("+"P"+cost+")";
            service_txt.setText(newlist);

            return rootview;
        }
    }

    ;
}*/
