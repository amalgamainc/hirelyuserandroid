package com.hirely.app.fragment;


import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hirely.app.R;
import com.hirely.app.util.PrefUtil;
import com.hirely.app.util.WebServiceHandler;

public class JobInProgressFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        new AsyncTask<Void, Void, String>() {

            String status = "4";

            @Override
            protected String doInBackground(Void... params) {
                return WebServiceHandler.updateUserStatus(getActivity(), status);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (s.equals("1")) {
                    PrefUtil.setUserStatus(getActivity(), status);
                }
            }
        }.execute();

        return inflater.inflate(R.layout.fragment_job_in_progress, container, false);
    }


}
