package com.hirely.app.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.MapFragment;
import com.hirely.app.R;

@SuppressLint("NewApi")
public class SupportFragment extends Fragment implements OnClickListener {

    TextView request_txt;
    View rootView;
    MapFragment mapFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_support, container, false);
        return rootView;

    }

    @Override
    public void onClick(View v) {
    }
}