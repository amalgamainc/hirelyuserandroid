package com.hirely.app.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.app.R;
import com.hirely.app.util.PrefUtil;
import com.hirely.app.util.ValidationUtil;
import com.hirely.app.util.WebServiceHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressLint("NewApi")
public class ProfileFragment extends Fragment {

    private static final String TAG = ProfileFragment.class.getSimpleName();

    private View root;
    private Spinner mCountryCodeSpinner;
    private EditText mEditUsername, mEditEmail, mEditMobileNumber;
    private TextView mSaveProfile;

    private String oldFullMobileNumber;

    private String username;
    private String mobileNumber;
    private String email;
    private String countryCode;

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            if (res.equals("1")) {
                getUserProfile();
            } else {
                Toast.makeText(getActivity(), "Request Failed", Toast.LENGTH_LONG).show();
            }
        }
    };

    private Runnable info = new Runnable() {
        @Override
        public void run() {
            String res = "";
            try {
                res = WebServiceHandler.UserInformation(getActivity(), PrefUtil.getUserId(getActivity()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            handler.sendMessage(msg);
        }
    };

    private Handler editProfileHandler = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            if (res.equals("1")) {
                Toast.makeText(getActivity(), "Your changes has been applied", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
    };

    private Runnable editProfile = new Runnable() {
        @Override
        public void run() {
            String res = "";
            try {
                res = WebServiceHandler.editUserProfile(getActivity(), PrefUtil.getUserId(getActivity()), username, email, mobileNumber, countryCode);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            editProfileHandler.sendMessage(msg);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_profile, container, false);

        init();
        getUserProfile();
        new Thread(info).start();

        return root;
    }

    private void getUserProfile() {
        String email = PrefUtil.getUserEmail(getActivity());
        String oldMobileNumber = PrefUtil.getUserMobileNumber(getActivity());
        String username = PrefUtil.getUserUsername(getActivity());
        String oldCountryCode = PrefUtil.getUserCountryCode(getActivity());
        oldFullMobileNumber = oldCountryCode + oldMobileNumber;

        mEditEmail.setText(email);
        mEditMobileNumber.setText(oldMobileNumber);
        mEditUsername.setText(username);
        mCountryCodeSpinner.setSelection(getIndexOfWhichEndWith(oldCountryCode));
    }

    private void init() {
        mEditUsername = (EditText) root.findViewById(R.id.username);
        mEditEmail = (EditText) root.findViewById(R.id.email_txt);
        mEditMobileNumber = (EditText) root.findViewById(R.id.mob_txt);

        mCountryCodeSpinner = (Spinner) root.findViewById(R.id.countryCodeSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.CountryCodes, R.layout.custom_spinner_item);
        adapter.setDropDownViewResource(R.layout.custom_spinner_item);
        mCountryCodeSpinner.setAdapter(adapter);

        mSaveProfile = (TextView) root.findViewById(R.id.saveProfile);
        mSaveProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = mEditUsername.getText().toString();
                mobileNumber = mEditMobileNumber.getText().toString();
                email = mEditEmail.getText().toString();
                countryCode = mCountryCodeSpinner.getSelectedItem().toString().split(", ")[1];
                String fullMobileNumber = countryCode + mobileNumber;

                if (isInputsValid(username, email, mobileNumber)) {
                    if (fullMobileNumber.equals(oldFullMobileNumber)) {
                        new Thread(editProfile).start();
                    } else {
                        Log.i(TAG, "Going to verify...");
                        SignUpVerifyPhoneFragment fragment = new SignUpVerifyPhoneFragment();
                        fragment.setArguments(packDataInBundle(username, email, mobileNumber, countryCode));
                        getActivity().getFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
                    }
                }
            }
        });
    }

    private int getIndexOfWhichEndWith(String countryCode) {
        List<String> countryCodesArray = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.CountryCodes)));
        for (String code : countryCodesArray) {
            if (code.endsWith(countryCode)) {
                return countryCodesArray.indexOf(code);
            }
        }
        return -1;
    }

    private boolean isInputsValid(String username, String email, String mobileNumber) {
        boolean isValid = false;
        if (!ValidationUtil.isUsernameValid(username)) {
            mEditUsername.setError("Enter only alpha character");
        } else if (!ValidationUtil.isEmailValid(email)) {
            mEditEmail.setError("Invalid Email");
        } else if (!ValidationUtil.isMobilePhoneValid(mobileNumber)) {
            mEditMobileNumber.setError("Invalid Password");
        } else {
            isValid = true;
        }
        return isValid;
    }

    private Bundle packDataInBundle(String username, String email, String mobileNumber, String countryCode) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("isUpdate", true);
        bundle.putString("username", username);
        bundle.putString("email", email);
        bundle.putString("phoneNumber", mobileNumber);
        bundle.putString("countryCode", countryCode);
        return bundle;
    }
}