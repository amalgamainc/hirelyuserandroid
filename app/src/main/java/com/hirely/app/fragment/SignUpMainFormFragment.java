package com.hirely.app.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.hirely.app.R;
import com.hirely.app.activity.MainActivity;
import com.hirely.app.service.GPSTracker;
import com.hirely.app.util.PrefUtil;
import com.hirely.app.util.ValidationUtil;

import org.brickred.socialauth.Profile;
import org.brickred.socialauth.android.DialogListener;
import org.brickred.socialauth.android.SocialAuthAdapter;
import org.brickred.socialauth.android.SocialAuthError;
import org.brickred.socialauth.android.SocialAuthListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SignUpMainFormFragment extends Fragment {

    // GENERAL
    private static final String TAG = SignUpMainFormFragment.class.getSimpleName();
    // GCM
    String GCM_SENDER_ID = "542173807799";
    GoogleCloudMessaging gcm;
    String regid;
    // SOCIAL LOGIN
    SocialAuthAdapter adapter;
    String token;
    Profile profileMap;
    // GPS
    GPSTracker gps;
    private Context c;
    // UI
    private EditText mUsernameEdit;
    private EditText mEmailEdit;
    private EditText mPasswordEdit;
    private Spinner mCountryCodeSpinner;
    private EditText mMobilePhoneEdit;
    private TextView mLoginText;
    private ProgressDialog pd;
    private RelativeLayout mGoogleLogin;
    private RelativeLayout mFacebookLogin;
    private CheckBox mRememberCheck;
    private CheckBox mReceiveUpdatesCheck;
    // FIELDS
    private String mUsername;
    private String mEmail;
    private String mPassword;
    private String mPhoneNumber;
    private String mCountryCode;

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        c = getActivity();

        // gps
        gps = new GPSTracker(c);
        getLatLon();

        // gcm
        gcm = GoogleCloudMessaging.getInstance(c);
        regid = getRegistrationId(c);
        registerInBackground();

        PrefUtil.setServiceType(c, "Massage");

        // social login
        adapter = new SocialAuthAdapter(new ResponseListner());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        gps.stopUsingGPS();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_main_form, container, false);

        mCountryCode = getCountryCode();

        mUsernameEdit = (EditText) view.findViewById(R.id.login_name);
        mPasswordEdit = (EditText) view.findViewById(R.id.userpassword);
        mEmailEdit = (EditText) view.findViewById(R.id.email_name);
        mCountryCodeSpinner = (Spinner) view.findViewById(R.id.countryCodeSpinner);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.CountryCodes, R.layout.custom_spinner_item);
        spinnerAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        mCountryCodeSpinner.setAdapter(spinnerAdapter);
        mCountryCodeSpinner.setSelection(getIndexOfWhichEndWith(mCountryCode));
        mMobilePhoneEdit = (EditText) view.findViewById(R.id.mobilePhoneEdit);
        mLoginText = (TextView) view.findViewById(R.id.loginText);
        mLoginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUsername = mUsernameEdit.getText().toString();
                mPassword = mPasswordEdit.getText().toString();
                mEmail = mEmailEdit.getText().toString();
                mPhoneNumber = mMobilePhoneEdit.getText().toString();
                mCountryCode = mCountryCodeSpinner.getSelectedItem().toString().split(", ")[1];
                if (isInputsValid(mUsername, mEmail, mPassword, mPhoneNumber)) {
                    SignUpVerifyPhoneFragment verifyPhoneFragment = new SignUpVerifyPhoneFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("username", mUsername);
                    bundle.putString("password", mPassword);
                    bundle.putString("email", mEmail);
                    bundle.putString("countryCode", mCountryCode);
                    bundle.putString("phoneNumber", mPhoneNumber);
                    verifyPhoneFragment.setArguments(bundle);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.signUpContentFrame, verifyPhoneFragment)
                            .commitAllowingStateLoss();
                }
            }
        });
        mGoogleLogin = (RelativeLayout) view.findViewById(R.id.googleLoginRelative);
        mGoogleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefUtil.setSocialLoginType(c, "googleplus");
                if (!PrefUtil.getGoogleStatus(c)) {
                    adapter.authorize(c, SocialAuthAdapter.Provider.GOOGLEPLUS);
                    adapter.enable(mGoogleLogin);
                } else {
                    startActivity(new Intent(c, MainActivity.class));
                }
            }
        });
        mFacebookLogin = (RelativeLayout) view.findViewById(R.id.facebookLoginRelative);
        mFacebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefUtil.setSocialLoginType(c, "facebook");
                if (!PrefUtil.getFacebookStatus(c)) {
                    adapter.authorize(c, SocialAuthAdapter.Provider.FACEBOOK);
                    adapter.enable(mFacebookLogin);
                } else {
                    startActivity(new Intent(c, MainActivity.class));
                }
            }
        });
        mRememberCheck = (CheckBox) view.findViewById(R.id.rememberCheck);
        mRememberCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO
            }
        });
        mReceiveUpdatesCheck = (CheckBox) view.findViewById(R.id.receiveUpdatesCheck);
        mReceiveUpdatesCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO
            }
        });

        return view;
    }

    private boolean isInputsValid(String username, String email, String password,
                                  String phoneNumber) {
        boolean isValid = false;
        if (!ValidationUtil.isUsernameValid(username)) {
            mUsernameEdit.setError("Enter only alpha character");
        } else if (!ValidationUtil.isEmailValid(email)) {
            mEmailEdit.setError("Invalid Email");
        } else if (!ValidationUtil.isPasswordValid(password)) {
            mPasswordEdit.setError("Invalid Password");
        } else if (!ValidationUtil.isMobilePhoneValid(phoneNumber)) {
            mMobilePhoneEdit.setError("Invalid Number");
        } else {
            isValid = true;
        }
        return isValid;
    }

    private String getCountryCode() {
        TelephonyManager manager = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
        String countryID = manager.getSimCountryIso().toUpperCase();
        String[] countryCodesFromResources = this.getResources().getStringArray(R.array.CountryCodes);
        String countryCode = "";
        for (String resCountryCode : countryCodesFromResources) {
            String[] g = resCountryCode.split(",");
            if (g[0].trim().equals(countryID.trim())) {
                Log.e(TAG, "Country code" + g[1]);
                countryCode = g[1];
                break;
            }
        }
        return countryCode;
    }

    private String getRegistrationId(Context context2) {
        String registrationId = PrefUtil.getUserDeviceId(c);
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        int registeredVersion = PrefUtil.getAppVersion(c);
        int currentVersion = getAppVersion(c);
        PrefUtil.setAppVersion(c, currentVersion);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void registerInBackground() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(c);
                    }
                    PrefUtil.setUserDeviceId(c, gcm.register(GCM_SENDER_ID));
                    Log.i(TAG, "Device registered, registration ID=" + PrefUtil.getUserDeviceId(c));
                } catch (IOException ex) {
                    Log.e(TAG, ex.getLocalizedMessage(), ex);
                }
                return msg;
            }

            @SuppressWarnings("unused")
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);
    }

    private void getLatLon() {
        PrefUtil.setProviderLongitude(c, String.valueOf(gps.getLongitude()));
        PrefUtil.setProviderLatitude(c, String.valueOf(gps.getLatitude()));
        Log.e("Your LOCATION", PrefUtil.getProviderLongitude(c) + ";" + PrefUtil.getProviderLatitude(c));
    }

    private int getIndexOfWhichEndWith(String countryCode) {
        List<String> countryCodesArray = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.CountryCodes)));
        for (String code : countryCodesArray) {
            if (code.endsWith(countryCode)) {
                return countryCodesArray.indexOf(code);
            }
        }
        return -1;
    }

    public class ResponseListner implements DialogListener {
        @Override
        public void onBack() {
        }

        @Override
        public void onCancel() {
        }

        @SuppressWarnings("unchecked")
        @Override
        public void onComplete(Bundle arg0) {
            token = adapter.getCurrentProvider().getAccessGrant().getKey();
            adapter.getUserProfileAsync(new ProfileDataListener());
        }

        @Override
        public void onError(SocialAuthError arg0) {
        }
    }

    @SuppressWarnings("rawtypes")
    class ProfileDataListener implements SocialAuthListener {
        @Override
        public void onError(SocialAuthError arg0) {
        }

        @SuppressWarnings("static-access")
        @Override
        public void onExecute(String arg0, Object arg1) {
            Log.d("Custom-UI", "Receiving Data");

            profileMap = adapter.getUserProfile();

            String loginType = PrefUtil.getSocialLoginType(c);

            if (loginType.equalsIgnoreCase("googleplus")) {
                PrefUtil.setGoogleId(c, profileMap.getValidatedId());
                PrefUtil.setGoogleEmail(c, profileMap.getEmail());
                PrefUtil.setGoogleName(c, profileMap.getFirstName());

                adapter.signOut(c, SocialAuthAdapter.Provider.GOOGLEPLUS.toString());
            } else if (loginType.equalsIgnoreCase("facebook")) {
                PrefUtil.setFacebookId(c, profileMap.getValidatedId());
                PrefUtil.setFacebookEmail(c, profileMap.getEmail());
                PrefUtil.setFacebookName(c, profileMap.getFirstName());

                adapter.signOut(c, SocialAuthAdapter.Provider.FACEBOOK.toString());
            }

            getFragmentManager().beginTransaction()
                    .replace(R.id.signUpContentFrame, new SignUpVerifyPhoneFragment())
                    .commitAllowingStateLoss();
        }
    }

}
