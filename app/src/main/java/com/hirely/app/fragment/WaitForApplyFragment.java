package com.hirely.app.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.app.R;
import com.hirely.app.util.WebServiceHandler;

public class WaitForApplyFragment extends Fragment {

    private ProgressDialog pd;

    @SuppressLint("HandlerLeak")
    Handler cancelJobHandler = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            pd.dismiss();
            if (res.equalsIgnoreCase("1")) {
                Fragment cancel = new MassageServiceFragment();
                getFragmentManager().beginTransaction().replace(R.id.content_frame, cancel).commitAllowingStateLoss();
            } else {
                Toast.makeText(getActivity(), "Some error occured..Please Try again..", Toast.LENGTH_LONG).show();
            }
        }
    };
    private Runnable cancelRequest = new Runnable() {
        @Override
        public void run() {
            String req = "";
            try {
                req = WebServiceHandler.cancelRequestUser(getActivity(), "0");
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = req;
            cancelJobHandler.sendMessage(msg);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_wait_for_apply, container, false);

        TextView cancel_txt = (TextView) rootView.findViewById(R.id.cancel_request_txt);
        cancel_txt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                pd = ProgressDialog.show(getActivity(), "", "Canceling....");
                new Thread(cancelRequest).start();
            }
        });

        return rootView;
    }
}