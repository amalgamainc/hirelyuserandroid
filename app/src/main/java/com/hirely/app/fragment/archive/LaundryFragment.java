/*
package com.hirely.app.fragment.archive;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.app.Global;
import com.hirely.app.R;
import com.hirely.app.fragment.LocatorServicesFragment;
import com.hirely.app.util.WebServiceHandler;

@SuppressLint("NewApi")
public class LaundryFragment extends Fragment implements OnClickListener {

    static int c = 1;
    TextView procced_txt, kilos_txt, cost_txt;
    EditText flr_edttxt, building_edttxt, street_edttxt, city_edttxt, landmark_edttxt;
    RelativeLayout increment_layout, decrment_layout;
    String flr_str, building_str, street_str, city_str, land_str, cost_str, kilo_str;
    View rootView;
    Global global;
    String[] extra_mString = new String[1];
    ProgressDialog pd;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            pd.dismiss();
            if (res.equalsIgnoreCase("1")) {
                Fragment loction = new LocatorServicesFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, loction).addToBackStack(null).commit();
            } else {
                Fragment loction = new LocatorServicesFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, loction).addToBackStack(null).commit();
                //Toast.makeText(getActivity(), "Error: "+global.getRegisterMsg(), 2000).show();
            }
        }
    };// handler
    //<=============================WEB SERVICE FOR LAUNDRY===========================>
    private Runnable laundry = new Runnable() {
        @Override
        public void run() {
            String res = "";
            try {
                Toast.makeText(getActivity(), global.getServicetype() + " " + flr_str + " " + building_str + " " + street_str
                        + " " + city_str + " " + land_str + " " + extra_mString + " " + cost_str, Toast.LENGTH_LONG).show();

                res = WebServiceHandler.createJob(getActivity(), global.getServicetype(),
                        flr_str, building_str, street_str, city_str, land_str, extra_mString, cost_str);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            handler.sendMessage(msg);
        }
    };// runnable

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.laundry_service, container, false);

        global = (Global) getActivity().getApplicationContext();
        idsFetch();
        onClickListners();

        return rootView;
    }

    private void onClickListners() {
        increment_layout.setOnClickListener(this);
        decrment_layout.setOnClickListener(this);
        increment_layout.setOnClickListener(this);
        decrment_layout.setOnClickListener(this);
        procced_txt.setOnClickListener(this);
    }

    private void idsFetch() {
        kilos_txt = (TextView) rootView.findViewById(R.id.kilos_txt);
        kilos_txt.setText(String.valueOf(c) + " " + "KILOS");

        increment_layout = (RelativeLayout) rootView.findViewById(R.id.laundryIncrement);
        decrment_layout = (RelativeLayout) rootView.findViewById(R.id.laundrydecrement);

        flr_edttxt = (EditText) rootView.findViewById(R.id.unit);
        building_edttxt = (EditText) rootView.findViewById(R.id.building_edt);
        street_edttxt = (EditText) rootView.findViewById(R.id.street_txt);
        city_edttxt = (EditText) rootView.findViewById(R.id.city_txt);
        landmark_edttxt = (EditText) rootView.findViewById(R.id.landmark_txt);
        cost_txt = (TextView) rootView.findViewById(R.id.cost_txt);
        cost_txt.setText(global.getEstimatedcost());
        procced_txt = (TextView) rootView.findViewById(R.id.proceed_txt);
    }

    //<==================VALIDATE METHOD ================>
    @SuppressWarnings("deprecation")
    private boolean Validate(String flr_str, String building_str, String street_str,
                             String city_str, String land_str, String kilo_str, String cost_str) {
        boolean isvalidated = false;
        if (flr_str.trim().length() == 0) {
            Toast.makeText(getActivity(), "Please enter Flr..", 2000).show();
        } else if (building_str.trim().length() == 0) {
            Toast.makeText(getActivity(), "Please enter building name..", 2000).show();
        } else if (street_str.trim().length() == 0) {
            Toast.makeText(getActivity(), "Please enter street..", 2000).show();
        } else if (city_str.trim().length() == 0) {
            Toast.makeText(getActivity(), "Please enter city..", 2000).show();
        } else if (land_str.trim().length() == 0) {
            Toast.makeText(getActivity(), "Please enter landmark..", 2000).show();
        } else if (kilo_str.trim().length() == 0) {
            Toast.makeText(getActivity(), "Please enter kilos.", 2000).show();
        } else if (cost_str.trim().length() == 0) {
            Toast.makeText(getActivity(), "Calculating Cost..", 2000).show();
        } else {
            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setTitle("Submit");
            alertDialog.setMessage(global.getServicetype() + " ," + flr_str + " ," + building_str + ", " + street_str
                    + ", " + city_str + " ," + land_str + " ," + extra_mString[0] + ", " + cost_str);
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    alertDialog.dismiss();
                    pd = ProgressDialog.show(getActivity(), "", "Saving...");
                    new Thread(null, laundry, "").start();
                }
            });
            alertDialog.show();


        }
        return isvalidated;
    }

    //<============================ End WEB SERVICE FOR CONDO CLEANING===========================>
    @Override
    public void onClick(View v) {
        int i = v.getId();

        if (i == R.id.laundryIncrement) {
            int value = ++c;
            kilos_txt.setText(String.valueOf(value) + " " + "KILOS");
        } else if (i == R.id.laundrydecrement) {
            if (c == 0) {
                Toast.makeText(getActivity(), "Please enter atleast 1 kilos", 2000).show();
            } else {
                int value = --c;
                kilos_txt.setText(String.valueOf(value) + " " + "KILOS");
            }
        } else if (i == R.id.proceed_txt) {
            flr_str = flr_edttxt.getText().toString();
            building_str = building_edttxt.getText().toString();
            street_str = street_edttxt.getText().toString();
            city_str = city_edttxt.getText().toString();
            land_str = landmark_edttxt.getText().toString();
            cost_str = cost_txt.getText().toString();
            kilo_str = kilos_txt.getText().toString();
            extra_mString[0] = kilo_str;

            String address = flr_str + " " + building_str + " " + street_str + " " + city_str + " " + land_str;
            global.setUserAddress(address); //setaddress globally
            global.setServicetype("laundry");
            Log.e("global address ", "" + global.getUserAddress());

            String quantity = extra_mString[0];
            global.setQuantity(quantity);

            if (Validate(flr_str, building_str, street_str, city_str, land_str, kilo_str, cost_str)) {

            }
        }
    }

    public void onPause() {
        super.onPause();
    }

}*/
