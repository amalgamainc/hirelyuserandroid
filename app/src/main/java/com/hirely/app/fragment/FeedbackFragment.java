package com.hirely.app.fragment;


import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.app.R;
import com.hirely.app.activity.MainActivity;
import com.hirely.app.util.PrefUtil;
import com.hirely.app.util.WebServiceHandler;

@SuppressLint("NewApi")
public class FeedbackFragment extends Fragment {

    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.obj.equals("1")) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                getActivity().startActivity(intent);
            } else {
                Toast.makeText(getActivity(), "Not enough money. Please topup before finishing job.", Toast.LENGTH_LONG).show();
            }
        }
    };

    private View view;

    private TextView submitFeedback;
    private TextView companyName;
    private TextView providerName;
    private TextView priceOfWork;
    private RatingBar ratingBar;
    private EditText comment;

    private Runnable sendFeedbackThread = new Runnable() {
        @Override
        public void run() {
            String res = WebServiceHandler.sendFeedback(
                    getActivity(),
                    PrefUtil.getServiceId(getActivity()),
                    getComment(),
                    getRating());
            if (res.equals("1")) {
                WebServiceHandler.updateUserStatus(getActivity(), "0");
            }
            Message msg = new Message();
            msg.obj = res;
            handler.sendMessage(msg);
        }
    };

    @SuppressLint("HandlerLeak")
    Handler getPriceOfWorkHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.obj.equals("1")) {
                priceOfWork.setText("₱" + PrefUtil.getPriceOfWork(getActivity()));
            } else {
                Toast.makeText(getActivity(), "Error to get price of work", Toast.LENGTH_LONG).show();
            }
        }
    };

    private Runnable getPriceOfWork = new Runnable() {
        @Override
        public void run() {
            String res = "";
            try {
                res = WebServiceHandler.getPriceOfWorkByServiceId(
                        getActivity(),
                        PrefUtil.getServiceId(getActivity()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            getPriceOfWorkHandler.sendMessage(msg);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_feedback, container, false);

        ratingBar = (RatingBar) view.findViewById(R.id.rating);

        comment = (EditText) view.findViewById(R.id.edit_comment);

        priceOfWork = (TextView) view.findViewById(R.id.priceOfWork);
        new Thread(getPriceOfWork).start();

        companyName = (TextView) view.findViewById(R.id.companyName);
        companyName.setText(PrefUtil.getProviderCompanyName(getActivity()));

        providerName = (TextView) view.findViewById(R.id.provider_name);
        providerName.setText(PrefUtil.getProviderUsername(getActivity()));

        submitFeedback = (TextView) view.findViewById(R.id.submit_feedback);
        submitFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(sendFeedbackThread).start();
            }
        });

        new AsyncTask<Void, Void, String>() {

            String status = "5";

            @Override
            protected String doInBackground(Void... params) {
                return WebServiceHandler.updateUserStatus(getActivity(), status);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (s.equals("1")) {
                    PrefUtil.setUserStatus(getActivity(), status);
                }
            }
        }.execute();

        return view;
    }

    private float getRating() {
        return ratingBar.getRating();
    }

    private String getComment() {
        return comment.getText().toString();
    }
}
