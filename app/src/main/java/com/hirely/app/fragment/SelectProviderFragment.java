package com.hirely.app.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hirely.app.R;
import com.hirely.app.entity.Provider;
import com.hirely.app.util.GoogleMapDistanceUtil;
import com.hirely.app.util.PrefUtil;
import com.hirely.app.util.WebServiceHandler;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("NewApi")
public class SelectProviderFragment extends Fragment implements LocationListener, OnMapReadyCallback {

    private static final String TAG = SelectProviderFragment.class.getSimpleName();

    private static View rootView;

    private List<Provider> mProviders = new ArrayList<>();
    private ListView mProvidersListView;
    private ProvidersListAdapter mAdapter;

    private LinearLayout mSearchLayout;
    private LinearLayout mProvidersLayout;

    private ProgressDialog pd;

    private GoogleMap googleMap;
    private LatLng position;

    private Handler createJobHandler = new Handler() {
        public void handleMessage(Message msg) {
            Boolean res = (Boolean) msg.obj;
            pd.dismiss();
            if (res) {
                new AsyncTask<Void, Void, String>() {

                    String status = "2";

                    @Override
                    protected String doInBackground(Void... params) {
                        return WebServiceHandler.updateUserStatus(getActivity(), status);
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);

                        if (s.equals("1")) {
                            PrefUtil.setUserStatus(getActivity(), status);

                            getFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, new WaitForApplyFragment())
                                    .commitAllowingStateLoss();
                        } else {
                            Toast.makeText(getActivity(), "Something went wrong during status update", Toast.LENGTH_SHORT).show();
                        }
                    }
                }.execute();
            } else {
                Toast.makeText(getActivity(), "Not enough money. Please topup before creating order.", Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }

        try {
            if (rootView == null) {
                rootView = inflater.inflate(R.layout.locator_layout, container, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        position = new LatLng(
                Double.parseDouble(PrefUtil.getUserLatitude(getActivity())),
                Double.parseDouble(PrefUtil.getUserLongitude(getActivity())));

        mProvidersListView = (ListView) rootView.findViewById(R.id.available_providers_list);
        mAdapter = new ProvidersListAdapter(getActivity(), R.layout.provider_item, mProviders);
        mProvidersListView.setAdapter(mAdapter);
        mProvidersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                Log.i(TAG, "Selected provider: " + mProviders.get(position));

                pd = ProgressDialog.show(getActivity(), "", "Sending request...");

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        PrefUtil.setProviderId(getActivity(), mProviders.get(position).getId());

                        boolean isSuccess = WebServiceHandler.createJob(
                                getActivity(),
                                PrefUtil.getProviderId(getActivity()),
                                PrefUtil.getUserId(getActivity()),
                                SelectProviderFragment.this.position.latitude,
                                SelectProviderFragment.this.position.longitude,
                                PrefUtil.getUserAddress(getActivity()),
                                PrefUtil.getQuantity(getActivity()),
                                PrefUtil.getMassageType(getActivity()));

                        Message message = new Message();
                        message.obj = isSuccess;
                        createJobHandler.sendMessage(message);
                    }
                }).start();


            }
        });
        mProvidersListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(
                                mAdapter.getItem(position).getLat(),
                                mAdapter.getItem(position).getLon()),
                        20));
                return false;
            }
        });

        mSearchLayout = (LinearLayout) rootView.findViewById(R.id.search_linear);
        mProvidersLayout = (LinearLayout) rootView.findViewById(R.id.providers_linear);

        return rootView;

    }

    private void initializeMap() {
        Log.i(TAG, "Load map");
        MapFragment mapFragment = MapFragment.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.map_container, mapFragment).commitAllowingStateLoss();
        mapFragment.getMapAsync(this);
    }

    public void onResume() {
        super.onResume();
        initializeMap();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (googleMap != null) {
            googleMap.clear();
            MarkerOptions mp = new MarkerOptions();
            mp.position(new LatLng(location.getLatitude(), location.getLongitude()));
            mp.title("You're here");
            mp.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
            googleMap.addMarker(mp);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12));
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onDestroyView() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.map_container);
        if (fragment != null) {
            Log.i(TAG, "Remove map");
            getFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
        super.onPause();
    }

    public void onPause() {
        super.onPause();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        Log.i(TAG, "Map ready");

        googleMap.addMarker(new MarkerOptions()
                .title("You're here")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                .position(position));

        googleMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(position, 12));

        new GetProvidersAsync().execute();
    }

    class GetProvidersAsync extends AsyncTask<Void, Void, List<Provider>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mSearchLayout.setVisibility(View.VISIBLE);
            mProvidersLayout.setVisibility(View.GONE);
        }

        @Override
        protected List<Provider> doInBackground(Void... params) {
            List<Provider> providers = WebServiceHandler.getAvailableProviders(getActivity(),
                    position.latitude,
                    position.longitude);
            return GoogleMapDistanceUtil.updateETA(providers, position);
        }

        @Override
        protected void onPostExecute(List<Provider> providers) {
            super.onPostExecute(providers);
            mSearchLayout.setVisibility(View.GONE);
            mProvidersLayout.setVisibility(View.VISIBLE);
            mProviders.clear();
            mProviders.addAll(providers);
            mAdapter.notifyDataSetChanged();

            for (Provider p : mProviders) {
                Log.d(TAG, "Add marker for " + p.getId());
                googleMap.addMarker(new MarkerOptions()
                        .title(p.getName() + " - " + p.getCompany())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.markerprovider))
                        .position(new LatLng(p.getLat(), p.getLon())));
            }
        }
    }

    class ProvidersListAdapter extends ArrayAdapter<Provider> {

        public ProvidersListAdapter(Context context, int resource, List<Provider> items) {
            super(context, resource, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            if (v == null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                v = vi.inflate(R.layout.provider_item, null);
            }

            Provider p = getItem(position);

            if (p != null) {
                TextView tt1 = (TextView) v.findViewById(R.id.text);
                tt1.setText(String.format(
                        "%s - %s\nETA: %s\nPrice per hour: P%.2f",
                        p.getName(), p.getCompany(), p.getEta(), p.getPrice()));
            }

            return v;
        }
    }
}