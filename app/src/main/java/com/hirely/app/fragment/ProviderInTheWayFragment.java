package com.hirely.app.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hirely.app.R;
import com.hirely.app.service.GcmIntentService;
import com.hirely.app.util.PrefUtil;
import com.hirely.app.util.WebServiceHandler;

@SuppressLint("NewApi")
public class ProviderInTheWayFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = ProviderInTheWayFragment.class.getSimpleName();

    private static View root;
    private GoogleMap map;
    private Marker mProviderMarker;

    private ProgressDialog progressDialog;
    @SuppressLint("HandlerLeak")
    Handler handlerrequest = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            progressDialog.dismiss();
            if (res.equalsIgnoreCase("1")) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new MassageServiceFragment())
                        .commitAllowingStateLoss();
            } else {
                Toast.makeText(getActivity(), "Some error occured..Please Try again..", Toast.LENGTH_LONG).show();
            }
        }
    };
    private TextView providerName, providerMobileNumber;
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            progressDialog.dismiss();
            if (res.equalsIgnoreCase("1")) {
                companyName.setText(PrefUtil.getProviderCompanyName(getActivity()));
                providerName.setText(PrefUtil.getProviderUsername(getActivity()));
                providerMobileNumber.setText(PrefUtil.getProviderMobileNumber(getActivity()));

                LatLng position = new LatLng(
                        Double.parseDouble(PrefUtil.getProviderLatitude(getActivity())),
                        Double.parseDouble(PrefUtil.getProviderLongitude(getActivity())));
                mProviderMarker = map.addMarker(new MarkerOptions()
                        .title("Provider")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.markerprovider))
                        .position(position));
                map.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(position, 12));
            } else {
                Toast.makeText(getActivity(), "Provider not found", Toast.LENGTH_LONG).show();
            }
        }
    };
    private TextView cancel_txt;
    private Runnable cancelRequest = new Runnable() {
        @Override
        public void run() {
            String req = "";
            try {
                req = WebServiceHandler.cancelAcceptUser(getActivity());
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = req;
            handlerrequest.sendMessage(msg);
        }
    };
    private Runnable providerStatus = new Runnable() {
        @Override
        public void run() {
            String res = "";
            try {
                res = WebServiceHandler.providerStatus(getActivity());
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            handler.sendMessage(msg);
        }
    };

    @SuppressLint("HandlerLeak")
    Handler getProviderRatingHandler = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            if (res.equalsIgnoreCase("1")) {
                providerRating.setRating(PrefUtil.getProviderRating(getActivity()));
            } else {
                Toast.makeText(getActivity(), "Error to get provider rating", Toast.LENGTH_LONG).show();
            }
        }
    };

    private Runnable getProviderRating = new Runnable() {
        @Override
        public void run() {
            String res = "";
            try {
                res = WebServiceHandler.getProviderRating(getActivity(), PrefUtil.getProviderId(getActivity()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            getProviderRatingHandler.sendMessage(msg);
        }
    };
    private RatingBar providerRating;
    private TextView companyName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (root != null) {
            ViewGroup parent = (ViewGroup) root.getParent();
            if (parent != null)
                parent.removeView(root);
        }

        try {
            if (root == null) {
                root = inflater.inflate(R.layout.fragment_provider_in_the_way, container, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        progressDialog = ProgressDialog.show(getActivity(), "", "Loading...");

        new Thread(new Runnable() {
            @Override
            public void run() {
                WebServiceHandler.updateUserStatus(getActivity(), "3");
            }
        }).start();

        init();

        initializeMap();

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                new BroadcastReceiver() {
                    public void onReceive(Context context, Intent intent) {
                        String type = intent.getExtras().getString("type");
                        if (GcmIntentService.Status.valueOf(type).equals(GcmIntentService.Status.UPDATED_PROVIDER_LOCATION)) {
                            String lat = intent.getExtras().getString("lat");
                            String lon = intent.getExtras().getString("lon");

                            Log.e(TAG, "Google map: " + map);

                            if (mProviderMarker != null) {
                                Log.i(TAG, "Update provider marker");
                                mProviderMarker.setPosition(new LatLng(Double.parseDouble(lat), Double.parseDouble(lon)));
                            } else {
                                Log.w(TAG, "Provider marker == null");
                            }
                        }
                    }
                },
                new IntentFilter(GcmIntentService.BROADCAST_ACTION));

        return root;
    }

    private void initializeMap() {
        Log.i(TAG, "Load map");
        MapFragment mapFragment = MapFragment.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.map_container_customer_review, mapFragment).commitAllowingStateLoss();
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onDestroyView() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.map_container_customer_review);
        if (fragment != null) {
            Log.i(TAG, "Remove map");
            getFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
        super.onPause();
    }

    private void init() {
        companyName = (TextView) root.findViewById(R.id.companyName);
        providerName = (TextView) root.findViewById(R.id.name_pro);
        providerMobileNumber = (TextView) root.findViewById(R.id.contact_pro);

        providerRating = (RatingBar) root.findViewById(R.id.rating);
        new Thread(getProviderRating).start();

        RelativeLayout call = (RelativeLayout) root.findViewById(R.id.rel_call);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + PrefUtil.getProviderMobileNumber(getActivity()))));
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getActivity(), "You don't have app to call", Toast.LENGTH_LONG).show();
                }
            }
        });

        RelativeLayout sms = (RelativeLayout) root.findViewById(R.id.rel_sms);
        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", PrefUtil.getProviderMobileNumber(getActivity()), null)));
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getActivity(), "You don't have app to send SMS", Toast.LENGTH_LONG).show();
                }
            }
        });

        cancel_txt = (TextView) root.findViewById(R.id.cancel_request_txt);
        cancel_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = ProgressDialog.show(getActivity(), "", "Canceling....");
                new Thread(cancelRequest).start();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        WebServiceHandler.updateUserStatus(getActivity(), "0");
                        PrefUtil.setUserStatus(getActivity(), "0");
                    }
                }).start();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;

        LatLng myPosition = new LatLng(
                Double.parseDouble(PrefUtil.getUserLatitude(getActivity())),
                Double.parseDouble(PrefUtil.getUserLongitude(getActivity())));

        new Thread(providerStatus).start();

        googleMap.addMarker(new MarkerOptions()
                .title("You're here")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                .position(myPosition));

    }
}