package com.hirely.app.fragment;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.app.R;
import com.hirely.app.SessionManager;
import com.hirely.app.activity.MainActivity;
import com.hirely.app.util.PrefUtil;
import com.hirely.app.util.RingCaptchaUtil;
import com.hirely.app.util.ValidationUtil;
import com.hirely.app.util.WebServiceHandler;
import com.thrivecom.ringcaptcha.RingcaptchaAPIController;
import com.thrivecom.ringcaptcha.lib.handlers.RingcaptchaHandler;
import com.thrivecom.ringcaptcha.lib.models.RingcaptchaResponse;

public class SignUpVerifyPhoneFragment extends Fragment {

    // GENERAL
    private static final String TAG = SignUpVerifyPhoneFragment.class.getSimpleName();
    private Context c;
    // UI
    private LinearLayout mSendLayout;
    private LinearLayout mVerifyLayout;
    private EditText mPhoneEdit;
    private Spinner mCountryCodeSpinner;
    private EditText mCodeEdit;
    private TextView mVerifyText;
    private TextView mSendText;
    private ProgressDialog pd;
    // FIELDS
    private String mUsername;
    private String mEmail;
    private String mPassword;
    private String mPhoneNumber;
    private String mCountryCode;
    private String mSocialLoginType;
    private Boolean mIsUpdate;
    // HANDLER
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            pd.dismiss();
            if (res.equalsIgnoreCase("1")) {
                SessionManager.createLoginSession(c, mEmail, mPassword);
                startActivity(new Intent(c, MainActivity.class));
            } else {
                Toast.makeText(c, "" + PrefUtil.getErrorMessage(c), Toast.LENGTH_LONG).show();
            }
        }
    };
    private Handler handlerForFacebookLogin = new Handler() {

        public void handleMessage(Message msg) {

            String res = msg.obj.toString();
            pd.dismiss();
            if (res.equalsIgnoreCase("true")) {
                startActivity(new Intent(c, MainActivity.class));
                PrefUtil.setFacebookStatus(c, true);

                Log.e("Facebook Login ", "SUCCESS");
            } else {
                Log.e("Facebbok Login ", "FAILED");
                Toast.makeText(c,
                        String.valueOf("Username or Password is Incorrect"), Toast.LENGTH_SHORT).show();
            }
        }
    };
    private Handler handlerForGooglePlus = new Handler() {

        public void handleMessage(Message msg) {

            String res = msg.obj.toString();
            pd.dismiss();

            if (res.equalsIgnoreCase("true")) {
                startActivity(new Intent(c, MainActivity.class));
                PrefUtil.setGoogleStatus(c, true);

                Log.e("Google Plus Login ", "SUCCESS");
            } else {
                Log.e("Google PLus Login ", "FAILED");
                Toast.makeText(c,
                        String.valueOf("Username or Password is Incorrect"), Toast.LENGTH_SHORT).show();
            }
        }
    };
    private Handler editProfileHandler = new Handler() {
        public void handleMessage(Message msg) {
            pd.dismiss();
            String res = msg.obj.toString();
            if (res.equals("1")) {
                getFragmentManager().beginTransaction().replace(R.id.content_frame, new ProfileFragment()).commitAllowingStateLoss();
                Toast.makeText(getActivity(), "Your changes has been applied", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
    };

    // RUNNABLE
    private Runnable editProfile = new Runnable() {
        @Override
        public void run() {
            String res = "";
            try {
                res = WebServiceHandler.editUserProfile(getActivity(), PrefUtil.getUserId(getActivity()), mUsername, mEmail, mPhoneNumber, mCountryCode);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            editProfileHandler.sendMessage(msg);
        }
    };
    private Runnable signUpUser = new Runnable() {
        @Override
        public void run() {
            String res = "";
            try {
                res = WebServiceHandler.createAccount(
                        c,
                        mUsername,
                        mPassword,
                        mEmail,
                        mCountryCode,
                        mPhoneNumber,
                        PrefUtil.getUserDeviceId(c),
                        "A",
                        PrefUtil.getUserLatitude(c),
                        PrefUtil.getUserLongitude(c),
                        "0");

            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            handler.sendMessage(msg);
        }
    };
    private Runnable threadGooglePlusLogin = new Runnable() {
        @Override
        public void run() {
            String res = "false";
            try {
                res = WebServiceHandler.loginGooglePlus(c,
                        PrefUtil.getGoogleName(c),
                        PrefUtil.getGoogleEmail(c),
                        PrefUtil.getGoogleId(c),
                        PrefUtil.getUserDeviceId(c),
                        "A",
                        PrefUtil.getUserLatitude(c),
                        PrefUtil.getUserLongitude(c),
                        "0",
                        mPhoneNumber,
                        mCountryCode);

            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            handlerForGooglePlus.sendMessage(msg);
        }
    };
    private Runnable threadFacebookLogin = new Runnable() {
        @Override
        public void run() {
            String res = "false";
            try {
                res = WebServiceHandler.loginFacebook(c,
                        PrefUtil.getFacebookName(c),
                        PrefUtil.getFacebookEmail(c),
                        PrefUtil.getFacebookId(c),
                        PrefUtil.getUserDeviceId(c),
                        "A",
                        PrefUtil.getUserLatitude(c),
                        PrefUtil.getUserLongitude(c),
                        "0",
                        mCountryCode,
                        mPhoneNumber);

            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            handlerForFacebookLogin.sendMessage(msg);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_verify_phone, container, false);

        c = getActivity();

        mSendLayout = (LinearLayout) view.findViewById(R.id.sendLayout);
        mCountryCodeSpinner = (Spinner) view.findViewById(R.id.countryCodeSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.CountryCodes, R.layout.custom_spinner_item);
        mCountryCodeSpinner.setAdapter(adapter);
        mPhoneEdit = (EditText) view.findViewById(R.id.phoneEdit);
        mSendText = (TextView) view.findViewById(R.id.sendText);
        mSendText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ValidationUtil.isMobilePhoneValid(mPhoneEdit.getText().toString())) {
                    mPhoneNumber = mPhoneEdit.getText().toString();
                    mCountryCode = mCountryCodeSpinner.getSelectedItem().toString().split(", ")[1];

                    RingCaptchaUtil.getInstance().sendSMSWithCode(c, mCountryCode + mPhoneNumber);

                    mSendLayout.setVisibility(View.GONE);
                    mVerifyLayout.setVisibility(View.VISIBLE);
                } else {
                    mPhoneEdit.setError("Wrong phone");
                }
            }
        });

        mVerifyLayout = (LinearLayout) view.findViewById(R.id.verifyLayout);
        mCodeEdit = (EditText) view.findViewById(R.id.codeEdit);
        mVerifyText = (TextView) view.findViewById(R.id.verifyText);
        mVerifyText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RingCaptchaUtil.getInstance().checkCodeFromSMS(c, new RingcaptchaHandler() {

                    @Override
                    public void onSuccess(RingcaptchaResponse o) {
                        Log.e(TAG, "verifyCaptchaWithCode " + o.toString());
                        Toast.makeText(c, "Verified", Toast.LENGTH_LONG).show();
                        RingcaptchaAPIController.setSMSHandler(null);

                        pd = ProgressDialog.show(c, "Please wait...", "Creating account...");
                        if (mSocialLoginType == null) {
                            if (mIsUpdate) {
                                new Thread(editProfile).start();
                            } else {
                                new Thread(signUpUser).start();
                            }
                        } else {
                            if (mSocialLoginType.equalsIgnoreCase("googleplus")) {
                                pd = ProgressDialog.show(c, "", "Loading...");
                                new Thread(threadGooglePlusLogin).start();
                            } else if (mSocialLoginType.equalsIgnoreCase("facebook")) {
                                pd = ProgressDialog.show(c, "", "Loading...");
                                new Thread(threadFacebookLogin).start();
                            }
                        }
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e(TAG, e.getLocalizedMessage(), e);
                        mCodeEdit.setError(e.getLocalizedMessage());
                    }
                }, mCodeEdit.getText().toString());
            }
        });

        if (getArguments() == null) {
            mSocialLoginType = PrefUtil.getSocialLoginType(c);
            mVerifyLayout.setVisibility(View.GONE);
            mSendLayout.setVisibility(View.VISIBLE);
        } else {
            mSendLayout.setVisibility(View.GONE);
            mVerifyLayout.setVisibility(View.VISIBLE);

            Log.e(TAG, getArguments().toString());
            Bundle bundle = getArguments();

            mIsUpdate = bundle.getBoolean("isUpdate", false);
            mUsername = bundle.getString("username");
            mEmail = bundle.getString("email");
            mPassword = bundle.getString("password");
            mPhoneNumber = bundle.getString("phoneNumber");
            mCountryCode = bundle.getString("countryCode");

            RingCaptchaUtil.getInstance().sendSMSWithCode(c, mCountryCode + mPhoneNumber);
        }

        return view;
    }

}
