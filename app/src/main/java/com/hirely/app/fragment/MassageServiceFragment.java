package com.hirely.app.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.hirely.app.R;
import com.hirely.app.adapter.PlaceAutocompleteAdapter;
import com.hirely.app.entity.Category;
import com.hirely.app.service.GPSTracker;
import com.hirely.app.util.PrefUtil;
import com.hirely.app.util.WebServiceHandler;

@SuppressLint("NewApi")
public class MassageServiceFragment extends Fragment implements OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = MassageServiceFragment.class.getSimpleName();
    private final String SERVICE_TYPE = "MASSAGE";
    protected GoogleApiClient mGoogleApiClient;
    private View view;
    private RelativeLayout increment_layout;
    private RelativeLayout decrement_layout;
    private ProgressDialog progressDialog;
    private AutoCompleteTextView autoEditAddress;
    private PlaceAutocompleteAdapter mAdapter;
    private EditText editLandmark;
    private TextView proceed;
    private TextView hours_txt;
    private TextView cost_txt;
    private int amountOfHours;
    private int limitOfHours;
    private double costForHour;
    private String address;
    private String landmark;
    private String cost;
    private String hours;
    private String[] massageHours = new String[1];
    private ArrayAdapter<CharSequence> adapter;

    @SuppressLint("HandlerLeak")
    Handler getMassageInfoHandler = new Handler() {
        public void handleMessage(Message msg) {
            Category category = (Category) msg.obj;
            if (category != null) {
                costForHour = category.getAvgPrice();
                cost_txt.setText("₱" + (double) Math.round(costForHour * 100) / 100);
                limitOfHours = category.getLimits().get("hours");
            } else {
                Toast.makeText(getActivity(), "Error when try to get info about massage", Toast.LENGTH_LONG).show();
            }
        }
    };
    private Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.massage_service, container, false);

        init();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Places.GEO_DATA_API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }

    private void init() {
        decrement_layout = (RelativeLayout) view.findViewById(R.id.massagedecrement);
        increment_layout = (RelativeLayout) view.findViewById(R.id.massageIncrement);

        hours_txt = (TextView) view.findViewById(R.id.hours_txt);
        hours_txt.setText(1 + " HOURS");

        autoEditAddress = (AutoCompleteTextView) view.findViewById(R.id.address_txt);
        // Register a listener that receives callbacks when a suggestion has been selected
        autoEditAddress.setOnItemClickListener(mAutocompleteClickListener);

        GPSTracker gps = new GPSTracker(getActivity());
        Location location = gps.getLocation();
        int locationDiff = 5;
        LatLngBounds bounds = new LatLngBounds(
                new LatLng(
                        location.getLatitude() - locationDiff,
                        location.getLongitude() - locationDiff),
                new LatLng(
                        location.getLatitude() + locationDiff,
                        location.getLongitude() + locationDiff));
        gps.stopUsingGPS();

        // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
        // the entire world.
        mAdapter = new PlaceAutocompleteAdapter(getActivity(), mGoogleApiClient, bounds, null);
        autoEditAddress.setAdapter(mAdapter);

        editLandmark = (EditText) view.findViewById(R.id.landmark_txt);
        cost_txt = (TextView) view.findViewById(R.id.estimatedcost_txt);

        new Thread(new Runnable() {

            @Override
            public void run() {
                Category category = null;
                try {
                    category = WebServiceHandler.getCategoryInfo(getActivity(), "Massage");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Message msg = new Message();
                msg.obj = category;
                getMassageInfoHandler.sendMessage(msg);
            }
        }).start();

        amountOfHours = 1;

        proceed = (TextView) view.findViewById(R.id.proceed_txt);
        proceed.setOnClickListener(this);

        increment_layout.setOnClickListener(this);
        decrement_layout.setOnClickListener(this);

        Spinner spinner = (Spinner) view.findViewById(R.id.massage_spinner);
        adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.massage_type_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PrefUtil.setMassageType(view.getContext(), adapter.getItem(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.massageIncrement) {
            if (amountOfHours == limitOfHours) {
                Toast.makeText(getActivity(), "You have reach the maximum number of hours", Toast.LENGTH_SHORT).show();
            } else {
                hours_txt.setText(++amountOfHours + " HOURS");
                cost_txt.setText("₱" + (double) Math.round(costForHour * amountOfHours * 100) / 100);
            }
        } else if (id == R.id.massagedecrement) {
            if (amountOfHours == 1) {
                Toast.makeText(getActivity(), "You have reach the minimum number of hours", Toast.LENGTH_SHORT).show();
            } else {
                hours_txt.setText(--amountOfHours + " HOURS");
                cost_txt.setText("₱" + (double) Math.round(costForHour * amountOfHours * 100) / 100);
            }
        }
        if (id == R.id.proceed_txt) {
            address = autoEditAddress.getText().toString();
            landmark = editLandmark.getText().toString();
            hours = hours_txt.getText().toString();
            cost = cost_txt.getText().toString().replace("₱", "");

            massageHours[0] = hours;

            if (validate(address, landmark, cost)) {
                displaySubmitDialog();
            }

        }
    }

    private void displaySubmitDialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_submit_job);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.alertdialog);
        dialog.show();

        TextView see_btn = (TextView) dialog.findViewById(R.id.okk_acepted);
        TextView dataText = (TextView) dialog.findViewById(R.id.dataSubmit);
        dataText.setText(String.format("Massage ,%s ,%s ,%s, %s", address, landmark, massageHours[0], cost));

        see_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                String fullAddress = address + " " + landmark;

                GPSTracker gps = new GPSTracker(getActivity());

                PrefUtil.setUserAddress(getActivity(), fullAddress);
                PrefUtil.setServiceType(getActivity(), SERVICE_TYPE);
                PrefUtil.setUserLatitude(getActivity(), String.valueOf(gps.getLatitude()));
                PrefUtil.setUserLongitude(getActivity(), String.valueOf(gps.getLongitude()));
                PrefUtil.setQuantity(getActivity(), String.valueOf(amountOfHours));

                gps.stopUsingGPS();

                Log.e(TAG, "Your location: " + PrefUtil.getUserLatitude(getActivity()) + ";" + PrefUtil.getUserLongitude(getActivity()));

                new AsyncTask<Void, Void, String>() {

                    String status = "1";

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        progressDialog = ProgressDialog.show(getActivity(), "", "Saving...");
                    }

                    @Override
                    protected String doInBackground(Void... params) {
                        return WebServiceHandler.updateUserStatus(getActivity(), status);
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        progressDialog.dismiss();

                        if (s.equals("1")) {
                            PrefUtil.setUserStatus(getActivity(), status);

                            getFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, new SelectProviderFragment())
                                    .commitAllowingStateLoss();
                        } else {
                            Toast.makeText(getActivity(), "Error when order massage", Toast.LENGTH_LONG).show();
                        }
                    }

                }.execute();
            }
        });

        dialog.show();
    }

    private boolean validate(String city_str, String land_str, String cost_str) {
        boolean isValid = false;
        if (city_str.trim().isEmpty()) {
            Toast.makeText(getActivity(), "Please enter address", Toast.LENGTH_SHORT).show();
        } else if (land_str.trim().isEmpty()) {
            Toast.makeText(getActivity(), "Please enter landmark", Toast.LENGTH_SHORT).show();
        } else if (hours.trim().isEmpty()) {
            Toast.makeText(getActivity(), "Please select hours", Toast.LENGTH_SHORT).show();
        } else if (cost_str.trim().isEmpty()) {
            Toast.makeText(getActivity(), "Calculating Cost", Toast.LENGTH_SHORT).show();
        } else if (amountOfHours == 0) {
            Toast.makeText(getActivity(), "Please enter at least 1 Hour", Toast.LENGTH_SHORT).show();
        } else {
            isValid = true;
        }
        return isValid;
    }

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(com.google.android.gms.common.api.GoogleApiClient,
     * String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            Log.i(TAG, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            Toast.makeText(getActivity(), "Clicked: " + primaryText,
                    Toast.LENGTH_SHORT).show();
            Log.i(TAG, "Called getPlaceById to get Place details for " + placeId);
        }
    };

    public void onPause() {
        super.onPause();
    }

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            final Place place = places.get(0);

            autoEditAddress.setText(place.getAddress());

            Log.i(TAG, "Place details received: " + address);

            places.release();
        }
    };

    /**
     * Called when the Activity could not connect to Google Play services and the auto manager
     * could resolve the error automatically.
     * In this case the API is not available and notify the user.
     *
     * @param connectionResult can be inspected to determine the cause of the failure
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.e(TAG, "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());

        // TODO(Developer): Check error code and notify the user of error state and resolution.
        Toast.makeText(getActivity(),
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }
}