/*
package com.hirely.app.fragment.archive;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.app.R;
import com.hirely.app.fragment.LocatorServicesFragment;
import com.hirely.app.util.WebServiceHandler;

@SuppressLint({"NewApi", "HandlerLeak"})
public class CandoCleaningFragment extends Fragment implements OnClickListener {

    TextView procced_txt_btn, bedrooms_value_txt, bathroom_txt, cost_txt;
    EditText flr_edttxt, building_edttxt, street_edttxt, city_edttxt, landmark_edttxt;
    String flr_str, building_str, street_str, city_str, land_str, cost_str, bed_str, bath_str;
    RelativeLayout increment_layout, decrment_layout, increment_bath, decrment_bath;
    int c = 0, d = 0;
    View rootView;
    String[] extra_mString = new String[2];
    ProgressDialog pd;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            pd.dismiss();
            Fragment loction = new LocatorServicesFragment();
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, loction).addToBackStack(null).commit();
        }
    };// handler
    // ****************************WEB SERVICE FOR CONDO
    // CLEANING********************************
    private Runnable condo = new Runnable() {
        @Override
        public void run() {
            String res = "";
            try {
                res = WebServiceHandler.createJob(getActivity(), global.getServicetype(), flr_str, building_str,
                        street_str, city_str, land_str, extra_mString, cost_str);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            handler.sendMessage(msg);
        }
    };// runnable

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_cando_cleaning_service, container, false);

        global = (Global) getActivity().getApplicationContext();

        idsFetch();
        onClickListnerss();

        return rootView;
    }

    @SuppressWarnings({"static-access", "deprecation"})
    @Override
    public void onClick(View v) {
        int i = v.getId();

        if (i == R.id.increment) {
            if (c <= 2) {
                int value = ++c;
                bedrooms_value_txt.setText(String.valueOf(value) + " " + "BEDROOMS");
            } else {
                Toast.makeText(getActivity(), "You have reached the maximum number of bedrooms", 2000).show();
            }
        } else if (i == R.id.decrement) {
            if (c == 0) {
                bedrooms_value_txt.setText(String.valueOf(c) + " " + "BEDROOMS");
            } else {
                int value = --c;
                bedrooms_value_txt.setText(String.valueOf(value) + " " + "BEDROOMS");
            }
        } else if (i == R.id.increment_bath) {
            if (d <= 1) {
                int value = ++d;
                bathroom_txt.setText(String.valueOf(value) + " " + "BATHROOMS");
            } else {
                Toast.makeText(getActivity(), "You have reached the maximum number of bathrooms", 2000).show();
            }
        } else if (i == R.id.decrement_bath) {
            if (d == 0) {
                bathroom_txt.setText(String.valueOf(d) + " " + "BATHROOMS");
            } else {
                int value = --d;
                bathroom_txt.setText(String.valueOf(value) + " " + "BATHROOMS");
            }
        } else if (i == R.id.proceed_txt) {
            flr_str = flr_edttxt.getText().toString();
            building_str = building_edttxt.getText().toString();
            street_str = street_edttxt.getText().toString();
            city_str = city_edttxt.getText().toString();
            land_str = landmark_edttxt.getText().toString();
            cost_str = cost_txt.getText().toString();
            bed_str = bedrooms_value_txt.getText().toString().trim();
            bath_str = bathroom_txt.getText().toString().trim();

            String address = flr_str + " " + building_str + " " + street_str + " " + city_str + " " + land_str;
            global.setUserAddress(address); // setaddress globally
            global.setServicetype("condo cleaning");
            Log.e("global address ", "" + global.getUserAddress());

            extra_mString[0] = bed_str;
            extra_mString[1] = bath_str;
            String quantity = extra_mString[0] + " " + extra_mString[1];
            global.setQuantity(quantity);

            if (flr_str.equals(" ")) {
                Toast.makeText(getActivity(), "Please enter flr..", 2000).show();
            } else if (building_str.equals(" ") || building_str.equals(null)) {
                Toast.makeText(getActivity(), "Please enter building..", 2000).show();
            } else if (street_str.equals(" ") || street_str.equals(null)) {

                Toast.makeText(getActivity(), "Please enter street..", 2000).show();
            } else if (city_str.equals(" ") || city_str.equals(null)) {
                Toast.makeText(getActivity(), "Please enter city..", 2000).show();
            } else if (land_str.equals(" ") || land_str.equals(null)) {
                Toast.makeText(getActivity(), "Please enter landmark..", 2000).show();
            } else if (bed_str.equals(" ") || bed_str.equals(null)) {
                Toast.makeText(getActivity(), "Please enter no of bedrooms.", 2000).show();
            } else if (bath_str.equals(" ") || bath_str.equals(null)) {
                Toast.makeText(getActivity(), "Please enter no of bathrooms..", 2000).show();
            } else if (c == 0 || d == 0) {
                Toast.makeText(getActivity(), "Please enter atleast 1 Bedroom or Bathroom", 2000).show();
            } else {
                final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Submit");
                alertDialog.setMessage(global.getServicetype() + " ," + flr_str + " ," + building_str + ", "
                        + street_str + ", " + city_str + " ," + land_str + " ," + extra_mString[0] + " "
                        + extra_mString[1] + ", " + cost_str);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        pd = ProgressDialog.show(getActivity(), "", "Saving...");
                        new Thread(null, condo, "").start();
                    }
                });
                alertDialog.show();

            }
        }
    }// onclick

    private void idsFetch() {
        increment_bath = (RelativeLayout) rootView.findViewById(R.id.increment_bath);
        decrment_bath = (RelativeLayout) rootView.findViewById(R.id.decrement_bath);
        increment_layout = (RelativeLayout) rootView.findViewById(R.id.increment);
        decrment_layout = (RelativeLayout) rootView.findViewById(R.id.decrement);

        flr_edttxt = (EditText) rootView.findViewById(R.id.unit);
        building_edttxt = (EditText) rootView.findViewById(R.id.building_edt);
        street_edttxt = (EditText) rootView.findViewById(R.id.street_txt);
        city_edttxt = (EditText) rootView.findViewById(R.id.city_txt);
        landmark_edttxt = (EditText) rootView.findViewById(R.id.landmark_txt);

        cost_txt = (TextView) rootView.findViewById(R.id.estimated_txt);
        cost_txt.setText(global.getEstimatedcost());

        bathroom_txt = (TextView) rootView.findViewById(R.id.bathroom_value);
        bathroom_txt.setText(String.valueOf(d) + " " + "BATHROOMS");

        bedrooms_value_txt = (TextView) rootView.findViewById(R.id.bedroom_value);
        bedrooms_value_txt.setText(String.valueOf(c) + " " + "BEDROOMS");

        procced_txt_btn = (TextView) rootView.findViewById(R.id.proceed_txt);
    }

    private void onClickListnerss() {
        increment_bath.setOnClickListener(this);
        decrment_bath.setOnClickListener(this);
        increment_layout.setOnClickListener(this);
        decrment_layout.setOnClickListener(this);
        procced_txt_btn.setOnClickListener(this);
    }

    public void onPause() {
        super.onPause();
    }

    ;
}*/
