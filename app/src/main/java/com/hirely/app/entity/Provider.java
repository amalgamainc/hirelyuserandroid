package com.hirely.app.entity;

/**
 * Created in GET-IT by chornenkyy@gmail.com on 11.02.2016.
 */
public class Provider {

    private String id;
    private String name;
    private String company;
    private double lat;
    private double lon;
    private double distance;
    private double price;
    private String eta;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    @Override
    public String toString() {
        return "Provider{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", company='" + company + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                ", distance=" + distance +
                ", price=" + price +
                ", eta='" + eta + '\'' +
                '}';
    }

}
