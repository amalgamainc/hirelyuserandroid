package com.hirely.app.entity;

import java.util.Map;

public class Category {

    private String name;
    private double avgPrice;
    private Map<String, Integer> limits;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAvgPrice() {
        return avgPrice;
    }

    public void setAvgPrice(double avgPrice) {
        this.avgPrice = avgPrice;
    }

    public Map<String, Integer> getLimits() {
        return limits;
    }

    public void setLimits(Map<String, Integer> limits) {
        this.limits = limits;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                ", avgPrice=" + avgPrice +
                ", limits=" + limits +
                '}';
    }
}
