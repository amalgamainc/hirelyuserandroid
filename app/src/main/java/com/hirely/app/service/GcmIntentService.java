package com.hirely.app.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.hirely.app.R;
import com.hirely.app.activity.MainActivity;
import com.hirely.app.receiver.GcmBroadcastReceiver;
import com.hirely.app.util.PrefUtil;

public class GcmIntentService extends IntentService {

    public static String BROADCAST_ACTION = "com.hirely.SHOW_DIALOG";

    private static final String TAG = GcmIntentService.class.getSimpleName();
    public static final int NOTIFICATION_ID = 2102016;

    public enum Status {
        JOB_ACCEPTED, UPDATED_PROVIDER_LOCATION, JOB_STARTED, JOB_FINISHED, JOB_DECLINED
    }

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();

        if (!extras.isEmpty()) {

            Log.i(TAG, "Received: " + extras.toString());

            String key = extras.getString("message");
            if (key != null) {
                Intent i = new Intent(BROADCAST_ACTION);
                i.putExtra("type", key);

                switch (Status.valueOf(key)) {
                    case JOB_ACCEPTED:
                        sendNotificationProvider("Your job was accepted");
                        PrefUtil.setUserStatus(this, "3");
                        break;
                    case UPDATED_PROVIDER_LOCATION:
                        i.putExtra("lat", extras.getString("lat"));
                        i.putExtra("lon", extras.getString("lon"));
                        break;
                    case JOB_STARTED:
                        sendNotificationProvider("Job started");
                        PrefUtil.setUserStatus(this, "4");
                        break;
                    case JOB_FINISHED:
                        sendNotificationProvider("Job finished");
                        PrefUtil.setUserStatus(this, "5");
                        break;
                    case JOB_DECLINED:
                        sendNotificationProvider("Job was declined by provider");
                        PrefUtil.setUserStatus(this, "1");
                        break;
                    default:
                        Log.e(TAG, "Unhandled value: " + key);
                }

                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);
            }

        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotificationProvider(String text) {
        if (PrefUtil.getAppInBackround(this)) {
            NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this).setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Hirely Provider")
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                    .setContentText(text);
            builder.setContentIntent(contentIntent);
            mNotificationManager.notify(NOTIFICATION_ID, builder.build());
            PrefUtil.setLastNotificationID(this, NOTIFICATION_ID);
        }
    }

}