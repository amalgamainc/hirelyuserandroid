package com.hirely.app.util;

import android.content.Context;
import android.util.Log;

import com.thrivecom.ringcaptcha.RingcaptchaAPIController;
import com.thrivecom.ringcaptcha.RingcaptchaService;
import com.thrivecom.ringcaptcha.lib.handlers.RingcaptchaHandler;
import com.thrivecom.ringcaptcha.lib.models.RingcaptchaResponse;

public class RingCaptchaUtil {
    private static final String TAG = RingCaptchaUtil.class.getSimpleName();
    // RING CAPTCHA KEYS
    private static final String APP_KEY = "hefa5y2a2i7i8i8a7a5e";
    private static final String APP_SECRET = "7y9i5o4ugu6itama6a1u";
    private static final String API_SECRET = "3c1a0f1c43dea60c3612db261f310da7695d4655";
    // OTHER FIELDS
    private static RingCaptchaUtil instance;
    private RingcaptchaAPIController mRingCaptchaAPIController;

    private RingCaptchaUtil() {
        mRingCaptchaAPIController = new RingcaptchaAPIController(APP_KEY);
    }

    public static RingCaptchaUtil getInstance() {
        if (instance == null) {
            instance = new RingCaptchaUtil();
        }
        return instance;
    }

    public void sendSMSWithCode(final Context c, String phoneNumber) {
        mRingCaptchaAPIController.sendCaptchaCodeToNumber(c, phoneNumber, RingcaptchaService.SMS, new RingcaptchaHandler() {
            @Override
            public void onSuccess(RingcaptchaResponse ringcaptchaResponse) {
            }

            @Override
            public void onError(Exception e) {
                Log.e(TAG, e.getLocalizedMessage(), e);
            }
        }, API_SECRET);
    }

    public void checkCodeFromSMS(Context c, RingcaptchaHandler handler, String code) {

        mRingCaptchaAPIController.verifyCaptchaWithCode(c, code, handler, API_SECRET);
    }
}

