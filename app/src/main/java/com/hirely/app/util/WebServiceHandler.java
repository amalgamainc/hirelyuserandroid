package com.hirely.app.util;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.hirely.app.GlobalConstants;
import com.hirely.app.entity.Category;
import com.hirely.app.entity.Provider;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebServiceHandler extends AbstractRequestService {

    private static final String TAG = WebServiceHandler.class.getSimpleName();
    private static final String SUCCESS = "success";

    // ------------------------------------SIGNUP API
    // ----------------------------------------------
    public static String createAccount(Context c, String username_mString, String email_mString,
                                       String password_mString, String code_mString, String verify_mString, String deviceid_mString,
                                       String devicetype_mString, String latitude_mString, String longitude_mString, String status_mString) {
        String result = "";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.USERNAME, username_mString)
                    .appendQueryParameter(GlobalConstants.EMAIL, email_mString)
                    .appendQueryParameter(GlobalConstants.PASSWORD, password_mString)
                    .appendQueryParameter(GlobalConstants.CODE, code_mString)
                    .appendQueryParameter(GlobalConstants.VERIFYMOB, verify_mString)
                    .appendQueryParameter(GlobalConstants.DEVICEID, deviceid_mString)
                    .appendQueryParameter(GlobalConstants.DEVICETYPE, devicetype_mString)
                    .appendQueryParameter(GlobalConstants.LATITUDE, latitude_mString)
                    .appendQueryParameter(GlobalConstants.LONGITUDE, longitude_mString)
                    .appendQueryParameter(GlobalConstants.STATUS, status_mString)
                    .appendQueryParameter("REGISTER", "REGISTER");
            String query = builder.build().getEncodedQuery();

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "createAccount response: " + response);

            JSONObject job = new JSONObject(response);
            String status = job.getString("status");
            if (status.equalsIgnoreCase("success")) {
                result = "1";
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject user = jary.getJSONObject(i);
                    Log.e("Username", "" + user.getString("username"));
                    PrefUtil.setUserId(c, user.getString("id"));
                    PrefUtil.setUserUsername(c, user.getString("username"));
                    PrefUtil.setUserEmail(c, user.getString("email"));
                    PrefUtil.setUserCountryCode(c, user.getString("code"));
                    PrefUtil.setUserMobileNumber(c, user.getString("mobileno"));
                    PrefUtil.setUserStatus(c, user.getString("user_status"));
                    Log.e("SIGN UP API", "User status: " + user.getString("user_status"));
                }
            } else {
                result = "0";
                PrefUtil.setErrorMessage(c, job.getString("message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // ----------------------------------------LOGIN API
    // -------------------------------------------
    public static String loginWithHirely(Context c, String login_username_mString, String login_password_mString,
                                         String deviceid_mString, String devicetype_mString, String latitude_mString, String longitude_mString,
                                         String usertype_mString) {

        String result = "";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.EMAIL, login_username_mString)
                    .appendQueryParameter(GlobalConstants.loginpassword, login_password_mString)
                    .appendQueryParameter(GlobalConstants.DEVICEID, deviceid_mString)
                    .appendQueryParameter(GlobalConstants.DEVICETYPE, devicetype_mString)
                    .appendQueryParameter(GlobalConstants.LATITUDE, latitude_mString)
                    .appendQueryParameter(GlobalConstants.LONGITUDE, longitude_mString)
                    .appendQueryParameter(GlobalConstants.USERTYPE, usertype_mString)
                    .appendQueryParameter("LOGIN", "LOGIN");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "Login query: " + query);

            openConnection(con, query);

            String response = getResponse(con);
            Log.d(TAG, "Login response: " + response);

            JSONObject job = new JSONObject(response);
            String status = job.getString("status");
            if (status.equalsIgnoreCase("sucessfully login")) {
                result = "1";
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject user = jary.getJSONObject(i);
                    PrefUtil.setUserId(c, user.getString("id"));
                    PrefUtil.setUserUsername(c, user.getString("username"));
                    PrefUtil.setUserEmail(c, user.getString("email"));
                    PrefUtil.setUserCountryCode(c, user.getString("code"));
                    PrefUtil.setUserMobileNumber(c, user.getString("mobileno"));
                    PrefUtil.setUserDeviceId(c, user.getString("device_id"));
                    PrefUtil.setUserLatitude(c, user.getString("lat"));
                    PrefUtil.setUserLongitude(c, user.getString("lon"));
                    PrefUtil.setUserStatus(c, user.getString("user_status"));
                }
            } else {
                result = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // ------------------------------------USER STATUS API
    // ----------------------------------------------
    public static String userStatus(Context c) {

        String result = "";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.USERID, PrefUtil.getUserId(c))
                    .appendQueryParameter("users_detail", "users_detail");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "User status query: " + query);

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "User status response: " + response);

            JSONObject job = new JSONObject(response);
            String status = job.getString("status");
            if (status.equalsIgnoreCase("sucess")) {
                result = "1";
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject user = jary.getJSONObject(i);
                    PrefUtil.setUserStatus(c, user.getString("user_status"));
                    PrefUtil.setUserUsername(c, user.getString("username"));
                    PrefUtil.setUserEmail(c, user.getString("email"));
                    PrefUtil.setUserMobileNumber(c, user.getString("mobileno"));
                    PrefUtil.setUserCountryCode(c, user.getString("code"));
                    Log.e("USER STATUS API", "user status =" + user.getString("user_status"));
                }
            } else {
                result = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    // <=============================PROFILE INFORMATION
    // =============================>
    public static String UserInformation(Context c, String userid_mString) {
        String result = "";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.USERID, userid_mString)
                    .appendQueryParameter("user", "user");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "User inforamtion query: " + query);

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "User inforamtion response: " + response);

            JSONObject job = new JSONObject(response);
            String status = job.getString("status");
            if (status.equalsIgnoreCase("success")) {
                result = "1";
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject user = jary.getJSONObject(i);
                    PrefUtil.setUserUsername(c, user.getString("username"));
                    PrefUtil.setUserEmail(c, user.getString("email"));
                    PrefUtil.setUserCountryCode(c, user.getString("code"));
                    PrefUtil.setUserMobileNumber(c, user.getString("mobileno"));
                }
            } else {
                result = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // ------------------------------------CANCEL REQUEST API
    // ----------------------------------------------
    public static String cancelRequestUser(Context c, String status_mString) {
        String result = "";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.USERID, PrefUtil.getUserId(c))
                    .appendQueryParameter(GlobalConstants.USERSTATUS, status_mString)
                    .appendQueryParameter("cancel_request", "cancel_request");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "Cancel request query: " + query);

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "Cancel request response: " + response);

            JSONObject job = new JSONObject(response);
            String status = job.getString("status");
            if (status.equalsIgnoreCase("Success")) {
                result = "1";
            } else {
                result = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String cancelAcceptUser(Context c) {
        String result = "";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("user_id", PrefUtil.getUserId(c))
                    .appendQueryParameter("cancel_accept_request", "cancel_accept_request");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "cancelAcceptUser query: " + query);

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "cancelAcceptUser response: " + response);

            JSONObject job = new JSONObject(response);
            String status = job.getString("status");
            if (status.equalsIgnoreCase("success")) {
                result = "1";
            } else {
                result = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // ------------------------------------FACEBOOK LOGIN API
    // ----------------------------------------------
    public static String loginFacebook(Context context, String fbUsername_mString, String fbEmail_mString,
                                       String fbUserId_mString, String fbdeviceid_mString, String fbDeviceType_mString, String fblat_mString,
                                       String fblon_mString, String fbstatus_mString, String phone,
                                       String countryCode) {
        String success = " ";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.SOCIALUSERNAME, fbUsername_mString)
                    .appendQueryParameter(GlobalConstants.SOCIALEMAIL, fbEmail_mString)
                    .appendQueryParameter(GlobalConstants.SOCIALUNIQUEID, fbUserId_mString)
                    .appendQueryParameter(GlobalConstants.DEVICEID, fbdeviceid_mString)
                    .appendQueryParameter(GlobalConstants.DEVICETYPE, fbDeviceType_mString)
                    .appendQueryParameter(GlobalConstants.LATITUDE, fblat_mString)
                    .appendQueryParameter(GlobalConstants.LONGITUDE, fblon_mString)
                    .appendQueryParameter(GlobalConstants.STATUS, fbstatus_mString)
                    .appendQueryParameter("mobileno", phone)
                    .appendQueryParameter("code", countryCode)
                    .appendQueryParameter("facebook", "facebook");
            String query = builder.build().getEncodedQuery();

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "loginFacebook response: " + response);

            JSONObject job = new JSONObject(response);
            String status = job.getString("status");
            if (status.equalsIgnoreCase("inserted")) {
                success = "true";
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject user = jary.getJSONObject(i);
                    Log.e("Username", "" + user.getString("username"));
                    PrefUtil.setUserId(context, user.getString("id"));
                    PrefUtil.setUserUsername(context, user.getString("username"));
                    PrefUtil.setUserEmail(context, user.getString("email"));
                    PrefUtil.setUserCountryCode(context, user.getString("code"));
                    PrefUtil.setUserMobileNumber(context, user.getString("mobileno"));
                    PrefUtil.setUserStatus(context, user.getString("user_status"));
                    Log.e("F. API user status", "" + user.getString("user_status"));
                }
            } else if (status.equalsIgnoreCase("updated")) {
                success = "true";
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject user = jary.getJSONObject(i);
                    Log.e("Username", "" + user.getString("username"));
                    PrefUtil.setUserId(context, user.getString("id"));
                    PrefUtil.setUserUsername(context, user.getString("username"));
                    PrefUtil.setUserEmail(context, user.getString("email"));
                    PrefUtil.setUserCountryCode(context, user.getString("code"));
                    PrefUtil.setUserMobileNumber(context, user.getString("mobileno"));
                    PrefUtil.setUserStatus(context, user.getString("user_status"));
                    Log.e("F. API user status", "" + user.getString("user_status"));
                }
            } else {
                success = "false";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    // ------------------------------------Google+ LOGIN API
    // ----------------------------------------------
    public static String loginGooglePlus(Context context, String gpUsername_mString, String gpEmail_mString,
                                         String gpUserId_mString, String gpdeviceid_mString, String gpDeviceType_mString, String gplat_mString,
                                         String gplon_mString, String gpstatus_mString, String phone,
                                         String countryCode) {
        String success = "false";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.SOCIALUSERNAME, gpUsername_mString)
                    .appendQueryParameter(GlobalConstants.SOCIALEMAIL, gpEmail_mString)
                    .appendQueryParameter(GlobalConstants.SOCIALUNIQUEID, gpUserId_mString)
                    .appendQueryParameter(GlobalConstants.DEVICEID, gpdeviceid_mString)
                    .appendQueryParameter(GlobalConstants.DEVICETYPE, gpDeviceType_mString)
                    .appendQueryParameter(GlobalConstants.LATITUDE, gplat_mString)
                    .appendQueryParameter(GlobalConstants.LONGITUDE, gplon_mString)
                    .appendQueryParameter(GlobalConstants.STATUS, gpstatus_mString)
                    .appendQueryParameter("mobileno", phone)
                    .appendQueryParameter("code", countryCode)
                    .appendQueryParameter("google", "google");
            String query = builder.build().getEncodedQuery();

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "loginGoogle response: " + response);

            JSONObject job = new JSONObject(response);
            String status = job.getString("status");
            if (status.equalsIgnoreCase("inserted")) {
                success = "true";
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject user = jary.getJSONObject(i);
                    Log.e("Username", "" + user.getString("username"));
                    PrefUtil.setUserId(context, user.getString("id"));
                    PrefUtil.setUserUsername(context, user.getString("username"));
                    PrefUtil.setUserEmail(context, user.getString("email"));
                    PrefUtil.setUserCountryCode(context, user.getString("code"));
                    PrefUtil.setUserMobileNumber(context, user.getString("mobileno"));
                    PrefUtil.setUserStatus(context, user.getString("user_status"));
                    Log.e("Google API user status", "" + user.getString("user_status"));
                }
            } else if (status.equalsIgnoreCase("updated")) {
                success = "true";
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject user = jary.getJSONObject(i);
                    Log.e("Username", "" + user.getString("username"));
                    PrefUtil.setUserId(context, user.getString("id"));
                    PrefUtil.setUserUsername(context, user.getString("username"));
                    PrefUtil.setUserEmail(context, user.getString("email"));
                    PrefUtil.setUserCountryCode(context, user.getString("code"));
                    PrefUtil.setUserMobileNumber(context, user.getString("mobileno"));
                    PrefUtil.setUserStatus(context, user.getString("user_status"));
                    Log.e("Google API user status", "" + user.getString("user_status"));
                }
            } else {
                success = "false";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }


    // ------------------------------------PROVIDER STATUS INFO API
    // ----------------------------------------------
    public static String providerStatus(Context c) {

        String result = "";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.PROVIDERID, PrefUtil.getProviderId(c))
                    .appendQueryParameter("provider_detail", "provider_detail");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "Provider status query: " + query);

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "Provider status response: " + response);

            JSONObject job = new JSONObject(response);
            String status = job.getString("status");
            if (status.equalsIgnoreCase("sucess")) {
                result = "1";
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject provider = jary.getJSONObject(i);
                    PrefUtil.setProviderUsername(c, provider.getString("username"));
                    String providerContact = provider.getString("code") + provider.getString("mobileno");
                    PrefUtil.setProviderCompanyName(c, provider.getString("company_name"));
                    PrefUtil.setProviderMobileNumber(c, providerContact);
                    PrefUtil.setProviderLatitude(c, provider.getString("lat"));
                    PrefUtil.setProviderLongitude(c, provider.getString("lon"));
                }
            } else {
                result = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    // ------------------------------------FORGET PASSWORD API
    // ----------------------------------------------

    public static String forgetPassword(Context c, String email_mString, String new_password_mString) {
        String result = "";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.FORGET_EMAIL, email_mString)
                    .appendQueryParameter(GlobalConstants.FORGET_PASS, new_password_mString)
                    .appendQueryParameter("Forgot_password", "Forgot_password");
            String query = builder.build().getEncodedQuery();

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "forgerPassword response: " + response);

            JSONObject job = new JSONObject(response);
            String status = job.getString("status");
            if (status.equalsIgnoreCase("updated")) {
                result = "1";
                JSONArray jary = job.getJSONArray("message");
                for (int i = 0; i < jary.length(); i++) {
                    JSONObject user = jary.getJSONObject(i);
                    PrefUtil.setUserStatus(c, user.getString("user_status"));
                    PrefUtil.setUserUsername(c, user.getString("username"));
                    PrefUtil.setUserEmail(c, user.getString("email"));
                    PrefUtil.setUserMobileNumber(c, user.getString("mobileno"));
                    PrefUtil.setUserCountryCode(c, user.getString("code"));
                    Log.e("USER FOGETPASSWORD API", "user status =" + user.getString("user_status"));
                }
            } else {
                result = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //SEND FEEDBACK
    public static String sendFeedback(Context c, String serviceId, String comment, float rating) {
        String result = "0";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter(GlobalConstants.SERVICE_ID, serviceId)
                    .appendQueryParameter(GlobalConstants.COMMENT, comment)
                    .appendQueryParameter(GlobalConstants.RATING, String.valueOf(rating))
                    .appendQueryParameter(GlobalConstants.IS_PROVIDER, "0")
                    .appendQueryParameter("feedback", "feedback");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "SEND FEEDBACK QUERY: " + query);

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "Response:" + response);

            JSONObject job = new JSONObject(response);
            String status = job.getString("status");
            if (status.equalsIgnoreCase("success")) {
                result = "1";
            } else {
                result = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String updateUserStatus(Context c, String stat) {
        String result = "";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("id", PrefUtil.getUserId(c))
                    .appendQueryParameter("status", stat)
                    .appendQueryParameter("update_user_status", "update_user_status");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "Update user status to: " + stat + " Query: " + query);

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "Update user status to: " + stat + " Response: " + response);

            JSONObject job = new JSONObject(response);
            String status = job.getString("status");
            if (status.equalsIgnoreCase("updated")) {
                result = "1";
            } else {
                result = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Category getCategoryInfo(Context c, String categoryName) {

        Category category = new Category();

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("cat_name", categoryName)
                    .appendQueryParameter("category_info", "category_info");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "Get category information query: " + query);

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "Get category information response: " + response);

            JSONObject categoryJSON = new JSONObject(response);
            String status = categoryJSON.getString("status");
            if (status.equalsIgnoreCase(SUCCESS)) {
                category.setName(categoryName);
                category.setAvgPrice(Double.valueOf(categoryJSON.getString("avg")));
                Map<String, Integer> limits = new HashMap<>();
                limits.put("hours", categoryJSON.getInt("max_count"));
                category.setLimits(limits);
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return category;
    }

    public static String editUserProfile(Context c, String userId, String username, String email, String mobileNumber, String countryCode) {
        String result = "";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("id", PrefUtil.getUserId(c))
                    .appendQueryParameter("username", username)
                    .appendQueryParameter("email", email)
                    .appendQueryParameter("code", countryCode)
                    .appendQueryParameter("mobileno", mobileNumber)
                    .appendQueryParameter("update_user_by_id", "update_user_by_id");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "Edit profile query: " + query);

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "Edit profile response: " + response);

            JSONObject job = new JSONObject(response);
            String status = job.getString("status");
            if (status.equalsIgnoreCase("updated")) {
                result = "1";
            } else {
                result = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getPriceOfWorkByServiceId(Context context, String serviceId) {
        String result = "";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("id", serviceId)
                    .appendQueryParameter("get_jobprice_by_service_id", "get_jobprice_by_service_id");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "Get price of work query: " + query);

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "Get price of work response: " + response);

            JSONObject object = new JSONObject(response);
            String status = object.getString("status");
            if (status.equalsIgnoreCase("success")) {
                result = "1";
                PrefUtil.setPriceOfWork(context, object.getString("message"));
            } else {
                result = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getProviderRating(Context context, String providerId) {
        String result = "";

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("id", providerId)
                    .appendQueryParameter("avg_provider_rate", "avg_provider_rate");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "Get provider rating query: " + query);

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "Get provider rating response: " + response);

            JSONObject object = new JSONObject(response);
            String status = object.getString("status");
            if (status.equalsIgnoreCase("success")) {
                result = "1";
                PrefUtil.setProviderRating(context, Float.valueOf(object.getString("avg")));
            } else {
                result = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<Provider> getAvailableProviders(Context c, double lat, double lon) {
        List<Provider> providers = new ArrayList<>();

        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("lat", String.valueOf(lat))
                    .appendQueryParameter("lon", String.valueOf(lon))
                    .appendQueryParameter("service_type", "Massage")
                    .appendQueryParameter("return_providers", "return_providers");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "getAvailableProviders query: " + query);

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "getAvailableProviders response: " + response);

            JSONObject job = new JSONObject(response);
            String status = job.getString("status");
            if (status.equalsIgnoreCase("success")) {
                JSONArray jary = job.getJSONArray("message");
                Provider provider;
                for (int i = 0; i < jary.length(); i++) {
                    provider = new Provider();
                    JSONObject providerJSON = jary.getJSONObject(i);
                    provider.setId(providerJSON.getString("id"));
                    provider.setName(providerJSON.getString("username"));
                    provider.setCompany(providerJSON.getString("company_name"));
                    provider.setLat(Double.parseDouble(providerJSON.getString("lat")));
                    provider.setLon(Double.parseDouble(providerJSON.getString("lon")));
                    provider.setDistance(Double.parseDouble(providerJSON.getString("distance")));
                    provider.setPrice(Double.parseDouble(providerJSON.getString("price")));
                    providers.add(provider);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return providers;
    }

    public static boolean createJob(Context c, String providerID, String userID, double lat, double lon, String address, String order, String massageType) {
        boolean isSuccess = false;
        try {
            HttpURLConnection con = getConnection();

            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("provider_id", providerID)
                    .appendQueryParameter("user_id", userID)
                    .appendQueryParameter("lat", String.valueOf(lat))
                    .appendQueryParameter("lon", String.valueOf(lon))
                    .appendQueryParameter("address", address)
                    .appendQueryParameter("order", order)
                    .appendQueryParameter("service_type", "Massage")
                    .appendQueryParameter("massage_type", massageType)
                    .appendQueryParameter("create_job", "create_job");
            String query = builder.build().getEncodedQuery();

            Log.i(TAG, "createJob query: " + query);

            openConnection(con, query);

            String response = getResponse(con);
            Log.i(TAG, "createJob response: " + response);

            JSONObject object = new JSONObject(response);
            String status = object.getString("status");
            isSuccess = status.equalsIgnoreCase("success");
            if (isSuccess) {
                PrefUtil.setServiceId(c, object.getString("service_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isSuccess;
    }

    private static HttpURLConnection getConnection() throws IOException {
        return getPostConnection(new URL(GlobalConstants.url));
    }

}