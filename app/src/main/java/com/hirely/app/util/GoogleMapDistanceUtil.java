package com.hirely.app.util;

import android.net.Uri;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.hirely.app.entity.Provider;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created in GET-IT by chornenkyy@gmail.com on 12.02.2016.
 */
public class GoogleMapDistanceUtil {

    private static final String TAG = GoogleMapDistanceUtil.class.getSimpleName();

    private GoogleMapDistanceUtil() {
    }

    public static List<Provider> updateETA(List<Provider> providers, LatLng location) {
        try {
            OkHttpClient client = new OkHttpClient();
            String url = "https://maps.googleapis.com/maps/api/distancematrix/json";
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("origins", String.format("%f,%f", location.latitude, location.longitude))
                    .appendQueryParameter("language", "en-UK")
                    .appendQueryParameter("key", "AIzaSyCwazkG7_TWs_wEUXcWhPdQN2QyvujL1sQ");
            String destinationsParam = "";
            for (Provider provider : providers) {
                destinationsParam += provider.getLat() + "," + provider.getLon() + "|";
            }
            destinationsParam = destinationsParam.substring(0, destinationsParam.lastIndexOf("|"));
            builder.appendQueryParameter("destinations", destinationsParam);
            String query = builder.build().getEncodedQuery();

            Request request = new Request.Builder()
                    .url(url + "?" + query)
                    .build();

            Log.i(TAG, "updateETA: " + url + "?" + query);

            Response response = client.newCall(request).execute();

            String responseString = response.body().string();
            Log.d(TAG, "updateETA response: " + responseString);
            JSONObject job = new JSONObject(responseString);
            String status = job.getString("status");

            if (status.equalsIgnoreCase("OK")) {
                JSONArray rows = job.getJSONArray("rows");
                for (int i = 0; i < rows.length(); i++) {
                    JSONObject row = rows.getJSONObject(i);
                    JSONArray elements = row.getJSONArray("elements");
                    for (int j = 0; j < elements.length(); j++) {
                        JSONObject item = elements.getJSONObject(j);
                        JSONObject distance = item.getJSONObject("duration");
                        Log.e(TAG, providers.get(j).getId() + " -- " + distance.getString("text"));
                        providers.get(j).setEta(distance.getString("text"));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return providers;
    }

}
