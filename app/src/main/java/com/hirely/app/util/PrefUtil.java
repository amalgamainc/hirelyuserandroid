package com.hirely.app.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PrefUtil {

    private PrefUtil() {
    }

    private static SharedPreferences getPrefs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setUserId(Context context, String userId) {
        getPrefs(context).edit().putString("userId", userId).commit();
    }

    public static String getUserId(Context context) {
        return getPrefs(context).getString("userId", null);
    }

    public static void setUserUsername(Context context, String username) {
        getPrefs(context).edit().putString("userUsername", username).commit();
    }

    public static String getUserUsername(Context context) {
        return getPrefs(context).getString("userUsername", null);
    }

    public static String getUserAddress(Context context) {
        return getPrefs(context).getString("userAddress", null);
    }

    public static void setUserAddress(Context context, String address) {
        getPrefs(context).edit().putString("userAddress", address).commit();
    }

    public static String getServiceType(Context context) {
        return getPrefs(context).getString("currentServiceType", null);
    }

    public static void setServiceType(Context context, String serviceType) {
        getPrefs(context).edit().putString("currentServiceType", serviceType).commit();
    }

    public static void setUserLongitude(Context context, String longitude) {
        getPrefs(context).edit().putString("userLongitude", longitude).commit();
    }

    public static String getUserLongitude(Context context) {
        return getPrefs(context).getString("userLongitude", null);
    }

    public static void setUserLatitude(Context context, String latitude) {
        getPrefs(context).edit().putString("userLatitude", latitude).commit();
    }

    public static String getUserLatitude(Context context) {
        return getPrefs(context).getString("userLatitude", null);
    }

    public static void setQuantity(Context context, String quantity) {
        getPrefs(context).edit().putString("quantity", quantity).commit();
    }

    public static String getQuantity(Context context) {
        return getPrefs(context).getString("quantity", null);
    }

    public static void setUserEmail(Context context, String email) {
        getPrefs(context).edit().putString("userEmail", email).commit();
    }

    public static String getUserEmail(Context context) {
        return getPrefs(context).getString("userEmail", null);
    }

    public static void setUserCountryCode(Context context, String countryCode) {
        getPrefs(context).edit().putString("countryCode", countryCode).commit();
    }

    public static String getUserCountryCode(Context context) {
        return getPrefs(context).getString("countryCode", null);
    }

    public static void setUserMobileNumber(Context context, String mobileNumber) {
        getPrefs(context).edit().putString("mobileNumber", mobileNumber).commit();
    }

    public static String getUserMobileNumber(Context context) {
        return getPrefs(context).getString("mobileNumber", null);
    }

    public static void setUserStatus(Context context, String userStatus) {
        getPrefs(context).edit().putString("userStatus", userStatus).commit();
    }

    public static String getUserStatus(Context context) {
        return getPrefs(context).getString("userStatus", null);
    }

    public static void setUserDeviceId(Context context, String userDeviceId) {
        getPrefs(context).edit().putString("userDeviceId", userDeviceId).commit();
    }

    public static String getUserDeviceId(Context context) {
        return getPrefs(context).getString("userDeviceId", null);
    }

    public static void setUserType(Context context, String userType) {
        getPrefs(context).edit().putString("userType", userType).commit();
    }

    public static String getUserType(Context context) {
        return getPrefs(context).getString("userType", null);
    }

    public static void setSocialLoginType(Context context, String loginType) {
        getPrefs(context).edit().putString("loginType", loginType).commit();
    }

    public static String getSocialLoginType(Context context) {
        return getPrefs(context).getString("loginType", null);
    }

    public static void setProviderId(Context context, String providerId) {
        getPrefs(context).edit().putString("providerId", providerId).commit();
    }

    public static String getProviderId(Context context) {
        return getPrefs(context).getString("providerId", null);
    }

    public static void setServiceId(Context context, String serviceId) {
        getPrefs(context).edit().putString("serviceId", serviceId).commit();
    }

    public static String getServiceId(Context context) {
        return getPrefs(context).getString("serviceId", null);
    }

    public static void setProviderUsername(Context context, String username) {
        getPrefs(context).edit().putString("providerUsername", username).commit();
    }

    public static String getProviderUsername(Context context) {
        return getPrefs(context).getString("providerUsername", null);
    }

    public static void setProviderMobileNumber(Context context, String providerContact) {
        getPrefs(context).edit().putString("providerMobileNumber", providerContact).commit();
    }

    public static String getProviderMobileNumber(Context context) {
        return getPrefs(context).getString("providerMobileNumber", null);
    }

    public static void setProviderLongitude(Context context, String longitude) {
        getPrefs(context).edit().putString("providerLongitude", longitude).commit();
    }

    public static String getProviderLongitude(Context context) {
        return getPrefs(context).getString("providerLongitude", null);
    }

    public static void setProviderLatitude(Context context, String latitude) {
        getPrefs(context).edit().putString("providerLatitude", latitude).commit();
    }

    public static String getProviderLatitude(Context context) {
        return getPrefs(context).getString("providerLatitude", null);
    }

    public static void setPriceOfWork(Context context, String priceOfWork) {
        getPrefs(context).edit().putString("priceOfWork", priceOfWork).commit();
    }

    public static String getPriceOfWork(Context context) {
        return getPrefs(context).getString("priceOfWork", null);
    }


    //// FACEBOOK
    public static void setFacebookId(Context c, String id) {
        getPrefs(c).edit().putString("facebookID", id).apply();
    }

    public static String getFacebookId(Context c) {
        return getPrefs(c).getString("facebookID", null);
    }

    public static void setFacebookEmail(Context c, String email) {
        getPrefs(c).edit().putString("facebookEmail", email).apply();
    }

    public static String getFacebookEmail(Context c) {
        return getPrefs(c).getString("facebookEmail", null);
    }

    public static void setFacebookName(Context c, String name) {
        getPrefs(c).edit().putString("facebookName", name).apply();
    }

    public static String getFacebookName(Context c) {
        return getPrefs(c).getString("facebookName", null);
    }

    public static void setFacebookStatus(Context c, boolean status) {
        getPrefs(c).edit().putBoolean("facebookStatus", status).apply();
    }

    public static boolean getFacebookStatus(Context c) {
        return getPrefs(c).getBoolean("facebookStatus", false);
    }

    //// GOOGLE
    public static void setGoogleId(Context c, String id) {
        getPrefs(c).edit().putString("facebookID", id).apply();
    }

    public static String getGoogleId(Context c) {
        return getPrefs(c).getString("facebookID", null);
    }

    public static void setGoogleEmail(Context c, String email) {
        getPrefs(c).edit().putString("facebookEmail", email).apply();
    }

    public static String getGoogleEmail(Context c) {
        return getPrefs(c).getString("facebookEmail", null);
    }

    public static void setGoogleName(Context c, String name) {
        getPrefs(c).edit().putString("facebookName", name).apply();
    }

    public static String getGoogleName(Context c) {
        return getPrefs(c).getString("facebookName", null);
    }

    public static void setGoogleStatus(Context c, boolean status) {
        getPrefs(c).edit().putBoolean("googleStatus", status).apply();
    }

    public static boolean getGoogleStatus(Context c) {
        return getPrefs(c).getBoolean("googleStatus", false);
    }

    public static void setAppVersion(Context c, int appVersion) {
        getPrefs(c).edit().putInt("appVersion", appVersion).apply();
    }

    public static int getAppVersion(Context c) {
        return getPrefs(c).getInt("appVersion", 0);
    }

    public static float getProviderRating(Context context) {
        return getPrefs(context).getFloat("providerRating", 0);
    }

    public static void setProviderRating(Context c, float providerRating) {
        getPrefs(c).edit().putFloat("providerRating", providerRating).apply();
    }

    public static String getProviderCompanyName(Context context) {
        return getPrefs(context).getString("companyName", null);
    }

    public static void setProviderCompanyName(Context context, String companyName) {
        getPrefs(context).edit().putString("companyName", companyName).apply();
    }

    public static void setAppInBackground(Context c, boolean isInBackground) {
        getPrefs(c).edit().putBoolean("appInBackground", isInBackground).apply();
    }

    public static boolean getAppInBackround(Context c) {
        return getPrefs(c).getBoolean("appInBackground", false);
    }

    public static void setLastNotificationID(Context c, int notificationID) {
        getPrefs(c).edit().putInt("lastNotificationID", notificationID).apply();
    }

    public static int getLastNotificationID(Context c) {
        return getPrefs(c).getInt("lastNotificationID", 0);
    }

    public static void setErrorMessage(Context c, String message) {
        getPrefs(c).edit().putString("errorMessage", message).apply();
    }

    public static String getErrorMessage(Context c) {
        return getPrefs(c).getString("errorMessage", null);
    }

    public static void setMassageType(Context c, String massageType) {
        getPrefs(c).edit().putString("massage_type", massageType).apply();
    }

    public static String getMassageType(Context c) {
        return getPrefs(c).getString("massage_type", "");
    }
}
