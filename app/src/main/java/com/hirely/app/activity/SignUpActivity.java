package com.hirely.app.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

import com.hirely.app.R;
import com.hirely.app.fragment.SignUpMainFormFragment;

public class SignUpActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.sign_up);
        getFragmentManager().beginTransaction()
                .replace(R.id.signUpContentFrame, new SignUpMainFormFragment())
                .commit();
    }

}