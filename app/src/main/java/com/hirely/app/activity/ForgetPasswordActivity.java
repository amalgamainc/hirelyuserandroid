package com.hirely.app.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.app.GlobalConstants;
import com.hirely.app.R;
import com.hirely.app.util.WebServiceHandler;

public class ForgetPasswordActivity extends Activity implements OnClickListener {

    EditText email_ed, password_ed;
    TextView submit_btn;
    String email_mString, pass_mString;
    ProgressDialog pd;
    SharedPreferences sharedpref;
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            pd.dismiss();
            if (res.equalsIgnoreCase("1")) {
                startActivity(new Intent(ForgetPasswordActivity.this, MainActivity.class));
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Error in forget password ", Toast.LENGTH_LONG).show();
            }
        }
    };
    //============================ FOGET PASSWORD API =========================================
    private Runnable forgetpass = new Runnable() {
        @Override
        public void run() {
            String res = "";
            try {
                res = WebServiceHandler.forgetPassword(ForgetPasswordActivity.this, email_mString, pass_mString);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            handler.sendMessage(msg);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.forgetpassword);
        sharedpref = getSharedPreferences(GlobalConstants.PREF_NAME, Context.MODE_PRIVATE);

        init();

    }

    private void init() {
        email_ed = (EditText) findViewById(R.id.email_forgetpass);
        password_ed = (EditText) findViewById(R.id.password_forgetpassword);
        submit_btn = (TextView) findViewById(R.id.submit_forgetpass);
        submit_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.submit_forgetpass) {
            email_mString = email_ed.getText().toString();
            pass_mString = password_ed.getText().toString();
            if (Validate(email_mString, pass_mString)) {

            }
        }
    }// oncreate ends

    private boolean Validate(String email_mString, String login_password_mString) {
        boolean isvalidated = false;
        if (email_mString.trim().length() == 0) {
            email_ed.setError("Invalid email");
        } else if (login_password_mString.trim().length() == 0) {
            password_ed.setError("Invalid Password");
        } else {
            pd = ProgressDialog.show(ForgetPasswordActivity.this, "", "Loading...");
            new Thread(null, forgetpass, " ").start();
        }
        return isvalidated;
    }

}// maine class