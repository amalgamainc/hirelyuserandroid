package com.hirely.app.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.hirely.app.R;
import com.hirely.app.SessionManager;
import com.hirely.app.service.GPSTracker;
import com.hirely.app.util.PrefUtil;
import com.hirely.app.util.WebServiceHandler;

import org.brickred.socialauth.Profile;
import org.brickred.socialauth.android.DialogListener;
import org.brickred.socialauth.android.SocialAuthAdapter;
import org.brickred.socialauth.android.SocialAuthAdapter.Provider;
import org.brickred.socialauth.android.SocialAuthError;
import org.brickred.socialauth.android.SocialAuthListener;

import java.io.IOException;

public class LogInActivity extends Activity implements OnClickListener {

    //Token variables
    public static final String PROPERTY_REG_ID = "registration_id";
    static final String TAG = "GCMDemo";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    //login variables
    TextView newUser_txt, sign_in_txt, sign_in_fb_txt, forget_txt;
    EditText user_name_edt, user_password_edt;
    String login_username_mString, login_password_mString;
    ProgressDialog pd;
    ImageView remember_img, forget_img;
    boolean isRememberMe, isForgotPassword;
    // SOCIAL LOGIN
    SocialAuthAdapter adapter;
    String token, fbusername_mString, fbmail_mString, fbid_mString;
    Profile profileMap;
    GoogleCloudMessaging gcm;
    Context context;
    String regId;
    String SENDER_ID = "542173807799";

    //Google map Latitude longitude
    GPSTracker gps;
    double latitude, longitude;
    String lat_str, long_str;

    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            pd.dismiss();
//            if (res.equalsIgnoreCase("1")) {
                startActivity(new Intent(LogInActivity.this, MainActivity.class));
                finish();
//            } else {
//                Toast.makeText(getApplicationContext(), "Invalid entries", Toast.LENGTH_LONG).show();
//            }
        }
    };

    Handler handlerForFacebookLogin = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            pd.dismiss();
            if (res.equalsIgnoreCase("true")) {
                startActivity(new Intent(LogInActivity.this, MainActivity.class));
                finish();
                Log.e("Facebook Login ", "SUCCESS");
            } else {
                Log.e("Facebbok Login ", "FAILED");
                Toast fillValues_mToast = Toast.makeText(getApplicationContext(),
                        String.valueOf("Username or Password is Incorrect"), Toast.LENGTH_SHORT);
                fillValues_mToast.show();
            }
        }
    };
    //==========================================================================================================
    //FACEBOOK API
    Runnable threadFacebookLogin = new Runnable() {
        @Override
        public void run() {
            String res = "false";
            try {
                res = WebServiceHandler.loginFacebook(LogInActivity.this, fbusername_mString, fbmail_mString, fbid_mString, PrefUtil.getUserDeviceId(context),
                        "A", String.valueOf(gps.getLatitude()), String.valueOf(gps.getLongitude()), "0", "", "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            handlerForFacebookLogin.sendMessage(msg);
        }
    };
    //============================ LOGIN API =========================================
    private Runnable signIn = new Runnable() {
        @Override
        public void run() {
            String res = "";
            try {
                res = WebServiceHandler.loginWithHirely(LogInActivity.this, login_username_mString, login_password_mString,
                        PrefUtil.getUserDeviceId(context), "A", String.valueOf(gps.getLatitude()), String.valueOf(gps.getLongitude()), PrefUtil.getUserType(context));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            handler.sendMessage(msg);
        }
    };

    //====================================================================
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.sign_in);

        gps = new GPSTracker(LogInActivity.this);
        context = getApplicationContext();
        adapter = new SocialAuthAdapter(new ResponseListner());

        PrefUtil.setUserType(context, "U"); // set user type
        getLatLon();
        init();

        gcm = GoogleCloudMessaging.getInstance(this);
        regId = getRegistrationId(context);
        registerInBackground();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gps.stopUsingGPS();
    }

    //========================================================================================
    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.new_user) {
            startActivity(new Intent(LogInActivity.this, SignUpActivity.class));
        } else if (i == R.id.remembr_img) {
            if (!isRememberMe) {
                remember_img.setImageResource(R.drawable.opt_true);
                isRememberMe = true;
            } else {
                remember_img.setImageResource(R.drawable.opt_false);
                isRememberMe = false;
            }
        } else if (i == R.id.forget_img) {
            if (!isForgotPassword) {
                forget_img.setImageResource(R.drawable.opt_true);
                isForgotPassword = true;
            } else {
                forget_img.setImageResource(R.drawable.opt_false);
                isForgotPassword = false;
            }
        } else if (i == R.id.sign_in_fb) {
            PrefUtil.setSocialLoginType(context, "facebook");
            Boolean boolValue = PrefUtil.getFacebookStatus(LogInActivity.this);
            if (!boolValue) {
                adapter.authorize(LogInActivity.this, Provider.FACEBOOK);
                adapter.enable(sign_in_fb_txt);
                adapter.signOut(getApplicationContext(), Provider.FACEBOOK.toString());
            } else {
                startActivity(new Intent(LogInActivity.this, MainActivity.class));
            }
        } else if (i == R.id.forgot_txt) {
            if (isForgotPassword) {
                startActivity(new Intent(LogInActivity.this, ForgetPasswordActivity.class));
            } else {
                Toast.makeText(getApplicationContext(), "please select forget option", Toast.LENGTH_SHORT).show();
            }
        } else if (i == R.id.signin_txt_login) {
            login_username_mString = user_name_edt.getText().toString();
            login_password_mString = user_password_edt.getText().toString();

            validate(login_username_mString, login_password_mString);
        }
    }

    private void validate(String login_username_mString, String login_password_mString) {
        if (login_username_mString.trim().length() == 0) {
            Toast.makeText(LogInActivity.this, "Please enter username...", Toast.LENGTH_SHORT).show();
        } else if (login_password_mString.trim().length() == 0) {
            Toast.makeText(LogInActivity.this, "Please enter password...", Toast.LENGTH_SHORT).show();
        } else {
            if (isRememberMe) {
                SessionManager.createLoginSession(this, login_username_mString, login_password_mString);
            }
            pd = ProgressDialog.show(LogInActivity.this, "", "Loading...");
            new Thread(null, signIn, " ").start();

        }
    }

    //=========================================================================================================
    // Generate Device ID
    private String getRegistrationId(Context context2) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(MainActivity.class.getSimpleName(), Context.MODE_PRIVATE);
    }
//===========================================================================================	 

    //============================================================================
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    //===============================================================================
    @SuppressWarnings({"unchecked", "rawtypes"})
    private void registerInBackground() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regId = gcm.register(SENDER_ID);
                    String devId = regId.toString();
                    PrefUtil.setUserDeviceId(context, devId);
                    msg = "Device registered, registration ID=" + regId;
                    storeRegistrationId(context, regId);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @SuppressWarnings("unused")
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);
    }

    private void getLatLon() {
        latitude = gps.getLatitude();
        longitude = gps.getLongitude();
        lat_str = Double.toString(latitude);
        PrefUtil.setUserLatitude(context, lat_str);
        long_str = Double.toString(longitude);
        PrefUtil.setUserLongitude(context, long_str);
        Log.e("Your LOCATION", "" + latitude + "" + longitude);
    }

    private void init() {
        user_name_edt = (EditText) findViewById(R.id.user_name_login);
        user_password_edt = (EditText) findViewById(R.id.user_password_login);
        sign_in_txt = (TextView) findViewById(R.id.signin_txt_login);
        sign_in_fb_txt = (TextView) findViewById(R.id.sign_in_fb);
        newUser_txt = (TextView) findViewById(R.id.new_user);
        remember_img = (ImageView) findViewById(R.id.remembr_img);
        forget_img = (ImageView) findViewById(R.id.forget_img);
        forget_txt = (TextView) findViewById(R.id.forgot_txt);

        sign_in_txt.setOnClickListener(this);
        sign_in_fb_txt.setOnClickListener(this);
        newUser_txt.setOnClickListener(this);
        remember_img.setOnClickListener(this);
        forget_img.setOnClickListener(this);
        forget_txt.setOnClickListener(this);
    }

    //RESPONSE LISTNER CLASS
    public class ResponseListner implements DialogListener {
        @Override
        public void onBack() {
        }

        @Override
        public void onCancel() {
        }

        @SuppressWarnings("unchecked")
        @Override
        public void onComplete(Bundle arg0) {
            token = adapter.getCurrentProvider().getAccessGrant().getKey();
            adapter.getUserProfileAsync(new ProfileDataListener());
        }

        @Override
        public void onError(SocialAuthError arg0) {
        }
    }

    @SuppressWarnings("rawtypes")
    class ProfileDataListener implements SocialAuthListener {
        @Override
        public void onError(SocialAuthError arg0) {
        }

        @SuppressWarnings("static-access")
        @Override
        public void onExecute(String arg0, Object arg1) {
            Log.d("Custom-UI", "Receiving Data");

            profileMap = adapter.getUserProfile();

            fbid_mString = profileMap.getValidatedId().toString();
            fbusername_mString = profileMap.getFirstName().toString();
            fbmail_mString = profileMap.getEmail().toString();

            pd = new ProgressDialog(LogInActivity.this).show(LogInActivity.this, "", "Loading...");
            pd.show();

            new Thread(null, threadFacebookLogin, "").start();
            //adapter.signOut(LogInActivity.this, Provider.FACEBOOK.toString());
        }
    }

    ;
}