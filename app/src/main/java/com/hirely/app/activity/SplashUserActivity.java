package com.hirely.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.hirely.app.R;
import com.hirely.app.SessionManager;
import com.hirely.app.util.CheckAppAvailability;

public class SplashUserActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splashuser);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CheckAppAvailability.isInternetOn(this)) {
            if (CheckAppAvailability.isGPSOn(this)) {
                startWorkWithApp();
            } else {
                CheckAppAvailability.showMessage(this, CheckAppAvailability.Type.GPS);
            }
        } else {
            CheckAppAvailability.showMessage(this, CheckAppAvailability.Type.INTERNET);
        }
    }

    private void startWorkWithApp() {
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SessionManager.isLoggedIn(SplashUserActivity.this)) {
                    startActivity(new Intent(SplashUserActivity.this, MainActivity.class));
                } else {
                    startActivity(new Intent(SplashUserActivity.this, LogInActivity.class));
                }
                finish();
            }
        }, 3000);
    }

}