package com.hirely.app.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hirely.app.R;
import com.hirely.app.SessionManager;
import com.hirely.app.fragment.FeedbackFragment;
import com.hirely.app.fragment.JobInProgressFragment;
import com.hirely.app.fragment.MassageServiceFragment;
import com.hirely.app.fragment.ProfileFragment;
import com.hirely.app.fragment.ProviderInTheWayFragment;
import com.hirely.app.fragment.SelectProviderFragment;
import com.hirely.app.fragment.SupportFragment;
import com.hirely.app.fragment.WaitForApplyFragment;
import com.hirely.app.service.GcmIntentService;
import com.hirely.app.util.PrefUtil;
import com.hirely.app.util.WebServiceHandler;

public class MainActivity extends Activity implements View.OnClickListener {

    private static String TAG = MainActivity.class.getSimpleName();
    RelativeLayout rlServices, rlProfile2, rlSupport, rlLogout;
    String username;
    TextView txtTitle;
    ProgressDialog pd;

    private Handler cancelJobHandler = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            if (res.equals("1")) {
                PrefUtil.setUserAddress(MainActivity.this, null);
                PrefUtil.setQuantity(MainActivity.this, null);

                getFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new MassageServiceFragment())
                        .commitAllowingStateLoss();
            } else {
                Toast.makeText(MainActivity.this, "Something goes wrong during status update", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            String res = msg.obj.toString();
            pd.dismiss();
            if (res.equalsIgnoreCase("1")) {
                username = PrefUtil.getUserUsername(MainActivity.this);
                txtTitle.setText(username);

                openFragmentByStatus();

            } else {
                Toast.makeText(getApplicationContext(), "Invalid entries", Toast.LENGTH_LONG).show();
            }
        }
    };
    private DrawerLayout mDrawerLayout;
    private RelativeLayout mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle, mTitle;
    private Dialog dialog;

    private Runnable userstatus = new Runnable() {
        @Override
        public void run() {
            String res = "";
            try {
                res = WebServiceHandler.userStatus(MainActivity.this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Message msg = new Message();
            msg.obj = res;
            handler.sendMessage(msg);
        }
    };

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String type = intent.getExtras().getString("type");
            GcmIntentService.Status status = GcmIntentService.Status.valueOf(type);
            if (status.equals(GcmIntentService.Status.JOB_ACCEPTED)) {
                switchFragment(new ProviderInTheWayFragment());
            } else if (status.equals(GcmIntentService.Status.JOB_STARTED)) {
                switchFragment(new JobInProgressFragment());
            } else if (status.equals(GcmIntentService.Status.JOB_FINISHED)) {
                switchFragment(new FeedbackFragment());
            } else if (status.equals(GcmIntentService.Status.JOB_DECLINED)) {
                switchFragment(new SelectProviderFragment());

                displayDeclinedDialog();
            }
        }
    };

    private void displayDeclinedDialog() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_declined_job);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.alertdialog);
        dialog.show();

        TextView see_btn = (TextView) dialog.findViewById(R.id.yes_btn);

        see_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void openFragmentByStatus() {
        Log.i(TAG, "User status: " + PrefUtil.getUserStatus(this));
        switch (PrefUtil.getUserStatus(this)) {
            case "0":
                switchFragment(new MassageServiceFragment());
                break;
            case "1":
                switchFragment(new SelectProviderFragment());
                break;
            case "2":
                switchFragment(new WaitForApplyFragment());
                break;
            case "3":
                switchFragment(new ProviderInTheWayFragment());
                break;
            case "4":
                switchFragment(new JobInProgressFragment());
                break;
            case "5":
                switchFragment(new FeedbackFragment());
                break;
            default:
                startActivity(new Intent(MainActivity.this, LogInActivity.class));
        }
    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initMenu();

        pd = ProgressDialog.show(MainActivity.this, "", "Loading...");
        new Thread(userstatus).start();

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (String.valueOf(PrefUtil.getLastNotificationID(MainActivity.this)).length() != 0) {
            manager.cancel(PrefUtil.getLastNotificationID(MainActivity.this));
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (RelativeLayout) findViewById(R.id.left_drawer);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);
        getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setIcon(android.R.color.transparent);
        getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorAccent)));

        mDrawerToggle = new ActionBarDrawerToggle(this, // host Activity
                mDrawerLayout, R.drawable.list, R.string.nav_open, R.string.nav_close) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                messageReceiver, new IntentFilter(GcmIntentService.BROADCAST_ACTION));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                mDrawerLayout.closeDrawer(mDrawerList);
            } else {
                mDrawerLayout.openDrawer(mDrawerList);
            }
        }
        return true;
    }

    private void initMenu() {
        rlServices = (RelativeLayout) findViewById(R.id.rlServices);
        rlProfile2 = (RelativeLayout) findViewById(R.id.rlProfile2);
        rlSupport = (RelativeLayout) findViewById(R.id.rlSupport);
        rlLogout = (RelativeLayout) findViewById(R.id.rlLogout);
        txtTitle = (TextView) findViewById(R.id.title_txt);
        rlServices.setOnClickListener(this);
        rlProfile2.setOnClickListener(this);
        rlSupport.setOnClickListener(this);
        rlLogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.rlServices) {
            openFragmentByStatus();
        } else if (v.getId() == R.id.rlProfile2) {
            switchFragment(new ProfileFragment());
        } else if (v.getId() == R.id.rlSupport) {
            switchFragment(new SupportFragment());
        } else if (v.getId() == R.id.rlLogout) {
            String socialLoginType = PrefUtil.getSocialLoginType(MainActivity.this);
            if (socialLoginType != null) {
                if (socialLoginType.equalsIgnoreCase("facebook")) {
                    PrefUtil.setFacebookStatus(this, false);
                    startActivity(new Intent(MainActivity.this, LogInActivity.class));
                } else if (socialLoginType.equalsIgnoreCase("googleplus")) {
                    PrefUtil.setGoogleStatus(this, false);
                    startActivity(new Intent(MainActivity.this, LogInActivity.class));
                }
            } else {
                SessionManager.logoutUser(this);
            }
        }
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    private void switchFragment(Fragment fragment) {
        FragmentManager manager = getFragmentManager();
        manager.beginTransaction().replace(R.id.content_frame, fragment).commitAllowingStateLoss();
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.content_frame);
        if (fragment instanceof SelectProviderFragment) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_cancel_job);
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.alertdialog);
            dialog.show();

            TextView ok_btn = (TextView) dialog.findViewById(R.id.yes_btn);
            ok_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Message message = new Message();
                            message.obj = WebServiceHandler.updateUserStatus(MainActivity.this, "0");
                            cancelJobHandler.sendMessage(message);
                        }
                    }).start();
                }
            });

            TextView no_btn = (TextView) dialog.findViewById(R.id.no_btn);
            no_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }
    }

    // ============================== END =============================
    @Override
    protected void onResume() {
        PrefUtil.setAppInBackground(this, false);
        Log.e("in Activity", "in resume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        PrefUtil.setAppInBackground(this, true);
        Log.e("in Activity", "in pause");
        super.onPause();
    }
}