package com.hirely.app.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

import com.hirely.app.R;

public class AlertDialogActivity extends Activity {
    AlertDialog builder;
    String message = "";
    String res = "0";
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(LayoutParams.FLAG_NOT_TOUCH_MODAL, LayoutParams.FLAG_NOT_TOUCH_MODAL);
        getWindow().setFlags(LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

        Intent i = getIntent();
        res = i.getStringExtra("type");

        Log.e("res value alert", "" + res);
        if (res.equalsIgnoreCase("1")) {
            dialog = new Dialog(AlertDialogActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_accept_job);
            //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.alertdialog);
            dialog.show();

            TextView see_btn = (TextView) dialog.findViewById(R.id.see_acepted);

            see_btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    startActivity(new Intent(AlertDialogActivity.this, MainActivity.class));
                }
            });
        } else {
            dialog = new Dialog(AlertDialogActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_complete_job);
            //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setBackgroundDrawableResource(R.drawable.alertdialog);
            dialog.show();

            TextView see_btn = (TextView) dialog.findViewById(R.id.see_completed);

            see_btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Intent intent = new Intent(AlertDialogActivity.this, MainActivity.class);
                    startActivity(intent);

                }
            });
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
            return true;
        }
        return false;
    }
}